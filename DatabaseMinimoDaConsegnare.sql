CREATE DATABASE  IF NOT EXISTS `projectworkvuoto` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `projectworkvuoto`;
-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: localhost    Database: projectworkvuoto
-- ------------------------------------------------------
-- Server version	8.0.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categorie`
--

DROP TABLE IF EXISTS `categorie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `categorie` (
  `id_Categoria` int NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) DEFAULT NULL,
  `descrizione` varchar(450) DEFAULT NULL,
  `immagine` text,
  `id_Genitore` int DEFAULT NULL,
  PRIMARY KEY (`id_Categoria`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categorie`
--

LOCK TABLES `categorie` WRITE;
/*!40000 ALTER TABLE `categorie` DISABLE KEYS */;
/*!40000 ALTER TABLE `categorie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `coupon`
--

DROP TABLE IF EXISTS `coupon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `coupon` (
  `id_Coupon` int NOT NULL,
  `nome` varchar(45) DEFAULT NULL,
  `tipo` varchar(45) DEFAULT NULL,
  `data` date DEFAULT NULL,
  `scadenza` date DEFAULT NULL,
  `importo` double DEFAULT NULL,
  `utente_id_utente` int DEFAULT NULL,
  PRIMARY KEY (`id_Coupon`),
  KEY `FK8sugikhxxe062khok3d9nxaar` (`utente_id_utente`),
  CONSTRAINT `FK8sugikhxxe062khok3d9nxaar` FOREIGN KEY (`utente_id_utente`) REFERENCES `utenti` (`id_Utente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `coupon`
--

LOCK TABLES `coupon` WRITE;
/*!40000 ALTER TABLE `coupon` DISABLE KEYS */;
/*!40000 ALTER TABLE `coupon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `giacenza`
--

DROP TABLE IF EXISTS `giacenza`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `giacenza` (
  `id_Giacenza` int NOT NULL AUTO_INCREMENT,
  `quantità` int DEFAULT NULL,
  `prodotto_id_prodotto` int DEFAULT NULL,
  `sede_id_sede` int DEFAULT NULL,
  PRIMARY KEY (`id_Giacenza`),
  KEY `FK34n8klb8v4hvy10lxpiwxkvce` (`prodotto_id_prodotto`),
  KEY `FKsrwnpus6ovc74cs1qi6u319i3` (`sede_id_sede`),
  CONSTRAINT `FK34n8klb8v4hvy10lxpiwxkvce` FOREIGN KEY (`prodotto_id_prodotto`) REFERENCES `prodotti` (`id_Prodotto`),
  CONSTRAINT `FKsrwnpus6ovc74cs1qi6u319i3` FOREIGN KEY (`sede_id_sede`) REFERENCES `sedi` (`id_Sede`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `giacenza`
--

LOCK TABLES `giacenza` WRITE;
/*!40000 ALTER TABLE `giacenza` DISABLE KEYS */;
/*!40000 ALTER TABLE `giacenza` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `immagini`
--

DROP TABLE IF EXISTS `immagini`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `immagini` (
  `id_Immagine` int NOT NULL,
  `url_Immagine` varchar(3950) DEFAULT NULL,
  `principale` int DEFAULT NULL,
  `prodotto_id_prodotto` int DEFAULT NULL,
  PRIMARY KEY (`id_Immagine`),
  KEY `FKf0w3lmmw3qmn93r948s7opj9o` (`prodotto_id_prodotto`),
  CONSTRAINT `FKf0w3lmmw3qmn93r948s7opj9o` FOREIGN KEY (`prodotto_id_prodotto`) REFERENCES `prodotti` (`id_Prodotto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `immagini`
--

LOCK TABLES `immagini` WRITE;
/*!40000 ALTER TABLE `immagini` DISABLE KEYS */;
/*!40000 ALTER TABLE `immagini` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `indirizzi`
--

DROP TABLE IF EXISTS `indirizzi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `indirizzi` (
  `id_Indirizzo` int NOT NULL,
  `via` varchar(45) DEFAULT NULL,
  `città` varchar(45) DEFAULT NULL,
  `provincia` varchar(45) DEFAULT NULL,
  `cap` int DEFAULT NULL,
  `note` varchar(45) DEFAULT NULL,
  `utente_id_utente` int DEFAULT NULL,
  PRIMARY KEY (`id_Indirizzo`),
  KEY `FKk0btx5i3hor7tq0hlbm5ktrmy` (`utente_id_utente`),
  CONSTRAINT `FKk0btx5i3hor7tq0hlbm5ktrmy` FOREIGN KEY (`utente_id_utente`) REFERENCES `utenti` (`id_Utente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `indirizzi`
--

LOCK TABLES `indirizzi` WRITE;
/*!40000 ALTER TABLE `indirizzi` DISABLE KEYS */;
/*!40000 ALTER TABLE `indirizzi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lista_promo`
--

DROP TABLE IF EXISTS `lista_promo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lista_promo` (
  `id_lista_promo` int NOT NULL,
  `prodotto_id_prodotto` int DEFAULT NULL,
  `promozione_id_promozione` int DEFAULT NULL,
  PRIMARY KEY (`id_lista_promo`),
  KEY `FKfqkciheb7sueqksvw2n8n2l92` (`prodotto_id_prodotto`),
  KEY `FKaua2j2pa5lpy3ef1aa3xi1glc` (`promozione_id_promozione`),
  CONSTRAINT `FKaua2j2pa5lpy3ef1aa3xi1glc` FOREIGN KEY (`promozione_id_promozione`) REFERENCES `promozioni` (`id_Promozione`),
  CONSTRAINT `FKfqkciheb7sueqksvw2n8n2l92` FOREIGN KEY (`prodotto_id_prodotto`) REFERENCES `prodotti` (`id_Prodotto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lista_promo`
--

LOCK TABLES `lista_promo` WRITE;
/*!40000 ALTER TABLE `lista_promo` DISABLE KEYS */;
/*!40000 ALTER TABLE `lista_promo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `marche`
--

DROP TABLE IF EXISTS `marche`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `marche` (
  `id_Marca` int NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) DEFAULT NULL,
  `grande` int DEFAULT '0',
  `immagine` text,
  PRIMARY KEY (`id_Marca`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marche`
--

LOCK TABLES `marche` WRITE;
/*!40000 ALTER TABLE `marche` DISABLE KEYS */;
/*!40000 ALTER TABLE `marche` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `metodi_pagamenti`
--

DROP TABLE IF EXISTS `metodi_pagamenti`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `metodi_pagamenti` (
  `id_metodo_pagamento` int NOT NULL,
  `nome` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_metodo_pagamento`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `metodi_pagamenti`
--

LOCK TABLES `metodi_pagamenti` WRITE;
/*!40000 ALTER TABLE `metodi_pagamenti` DISABLE KEYS */;
/*!40000 ALTER TABLE `metodi_pagamenti` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ordini`
--

DROP TABLE IF EXISTS `ordini`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ordini` (
  `id_Ordine` int NOT NULL,
  `data_Creazione` date DEFAULT NULL,
  `data_Invio` date DEFAULT NULL,
  `data_Consegna` date DEFAULT NULL,
  `stato_ordine_id_stato_ordine` int DEFAULT NULL,
  PRIMARY KEY (`id_Ordine`),
  KEY `ccccc_idx` (`stato_ordine_id_stato_ordine`),
  CONSTRAINT `ccccc` FOREIGN KEY (`stato_ordine_id_stato_ordine`) REFERENCES `stato_ordini` (`id_stato_ordine`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ordini`
--

LOCK TABLES `ordini` WRITE;
/*!40000 ALTER TABLE `ordini` DISABLE KEYS */;
/*!40000 ALTER TABLE `ordini` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pagamenti`
--

DROP TABLE IF EXISTS `pagamenti`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pagamenti` (
  `id_Pagamento` int NOT NULL AUTO_INCREMENT,
  `data_Pagamento` date DEFAULT NULL,
  `importo` double DEFAULT NULL,
  `tipo_pagamento_id_tipo_pagamento` int DEFAULT NULL,
  PRIMARY KEY (`id_Pagamento`),
  KEY `FKhj8pivuwhbvmqbvrvvt9fuu1m` (`tipo_pagamento_id_tipo_pagamento`),
  CONSTRAINT `FKhj8pivuwhbvmqbvrvvt9fuu1m` FOREIGN KEY (`tipo_pagamento_id_tipo_pagamento`) REFERENCES `tipi_pagamenti` (`id_tipo_pagamento`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pagamenti`
--

LOCK TABLES `pagamenti` WRITE;
/*!40000 ALTER TABLE `pagamenti` DISABLE KEYS */;
/*!40000 ALTER TABLE `pagamenti` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prodotti`
--

DROP TABLE IF EXISTS `prodotti`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `prodotti` (
  `id_Prodotto` int NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) DEFAULT NULL,
  `breve_Descrizione` varchar(100) DEFAULT NULL,
  `descrizione_Completa` varchar(600) DEFAULT NULL,
  `prezzo` double DEFAULT NULL,
  `dimensione` varchar(45) DEFAULT NULL,
  `peso` double DEFAULT NULL,
  `rumorosità` int DEFAULT NULL,
  `materiale` varchar(45) DEFAULT NULL,
  `capacità` int DEFAULT NULL,
  `wattaggio` int DEFAULT NULL,
  `categoria_id_categoria` int DEFAULT NULL,
  `marca_id_marca` int DEFAULT NULL,
  PRIMARY KEY (`id_Prodotto`),
  KEY `FKfh81bhqvmxa26adfsfbru6qny` (`categoria_id_categoria`),
  KEY `FKrwv8x1d0lqjxua06slbdfbqe2` (`marca_id_marca`),
  CONSTRAINT `FKfh81bhqvmxa26adfsfbru6qny` FOREIGN KEY (`categoria_id_categoria`) REFERENCES `categorie` (`id_Categoria`),
  CONSTRAINT `FKrwv8x1d0lqjxua06slbdfbqe2` FOREIGN KEY (`marca_id_marca`) REFERENCES `marche` (`id_Marca`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prodotti`
--

LOCK TABLES `prodotti` WRITE;
/*!40000 ALTER TABLE `prodotti` DISABLE KEYS */;
/*!40000 ALTER TABLE `prodotti` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `promozioni`
--

DROP TABLE IF EXISTS `promozioni`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `promozioni` (
  `id_Promozione` int NOT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `descrizione` varchar(255) DEFAULT NULL,
  `immagine` varchar(255) DEFAULT NULL,
  `data_Inizio` date DEFAULT NULL,
  `data_Fine` date DEFAULT NULL,
  `percentuale_Sconto` int DEFAULT NULL,
  PRIMARY KEY (`id_Promozione`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `promozioni`
--

LOCK TABLES `promozioni` WRITE;
/*!40000 ALTER TABLE `promozioni` DISABLE KEYS */;
/*!40000 ALTER TABLE `promozioni` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ruoli`
--

DROP TABLE IF EXISTS `ruoli`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ruoli` (
  `id_Ruolo` int NOT NULL,
  `nome_Ruolo` varchar(45) DEFAULT NULL,
  `descrizione` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_Ruolo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ruoli`
--

LOCK TABLES `ruoli` WRITE;
/*!40000 ALTER TABLE `ruoli` DISABLE KEYS */;
INSERT INTO `ruoli` VALUES (1,'Utente','Utente base'),(2,'Amministratore','Accesso al lato back-end');
/*!40000 ALTER TABLE `ruoli` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sedi`
--

DROP TABLE IF EXISTS `sedi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sedi` (
  `id_Sede` int NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) DEFAULT NULL,
  `via` varchar(45) DEFAULT NULL,
  `città` varchar(45) DEFAULT NULL,
  `provincia` varchar(45) DEFAULT NULL,
  `cap` varchar(45) DEFAULT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_Sede`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sedi`
--

LOCK TABLES `sedi` WRITE;
/*!40000 ALTER TABLE `sedi` DISABLE KEYS */;
INSERT INTO `sedi` VALUES (1,'Sede Centrale','Via Amerigo Vespucci, 1','Gallarate','va','21013','0331706011','kitcheninlove@kitcheninlove.it'),(2,'Milano','Viale Coni Zugna, 23','Milano','mi','20100','0231706011','milano@kitcheninlove.it'),(3,'Roma','Via Roma, 1','Roma','roma','100','04123567','roma@kitcheninlove.it'),(4,'Firenze','Via Firenze, 1','Firenze','fi','50100','05527571','firenze@kitcheninlove.it'),(5,'Venezia','Via Lorem Ipsum, 1','Venezia','ve','30100','01234567','venezia@kitcheninlove.it'),(6,'Bologna','Via Lorem Ipsum, 1','Bologna','bo','40121','01234567','bologna@kitcheninlove.it'),(7,'Bari','Via Lorem Ipsum, 1','Bari','ba','70121','01234567','bari@kitcheninlove.it'),(8,'Napoli','Via Lorem Ipsum, 1','Napoli','na','80100','01234567','napoli@kitcheninlove.it'),(9,'Catania','Via Lorem Ipsum, 1','Catania','ct','95100','01234567','catania@kitcheninlove.it'),(10,'Cagliari','Via Lorem Ipsum, 1','Cagliari','ca','09121','01234567','cagliari@kitcheninlove.it'),(11,'Catanzaro','Via Lorem Ipsum, 1','Catanzaro','cz','88100','01234567','catanzaro@kitcheninlove.it'),(12,'Torino','Via Lorem Ipsum, 1','Torino','to','10100','01234567','torino@kitcheninlove.it'),(13,'Pescara','Via Lorem Ipsum, 1','Pescara','PE','65121','01234567','pescara@kitcheninlove.it');
/*!40000 ALTER TABLE `sedi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seq_categoria`
--

DROP TABLE IF EXISTS `seq_categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `seq_categoria` (
  `next_val` bigint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seq_categoria`
--

LOCK TABLES `seq_categoria` WRITE;
/*!40000 ALTER TABLE `seq_categoria` DISABLE KEYS */;
/*!40000 ALTER TABLE `seq_categoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seq_coupon`
--

DROP TABLE IF EXISTS `seq_coupon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `seq_coupon` (
  `next_val` bigint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seq_coupon`
--

LOCK TABLES `seq_coupon` WRITE;
/*!40000 ALTER TABLE `seq_coupon` DISABLE KEYS */;
/*!40000 ALTER TABLE `seq_coupon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seq_giacenza`
--

DROP TABLE IF EXISTS `seq_giacenza`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `seq_giacenza` (
  `next_val` bigint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seq_giacenza`
--

LOCK TABLES `seq_giacenza` WRITE;
/*!40000 ALTER TABLE `seq_giacenza` DISABLE KEYS */;
/*!40000 ALTER TABLE `seq_giacenza` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seq_immagine`
--

DROP TABLE IF EXISTS `seq_immagine`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `seq_immagine` (
  `next_val` bigint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seq_immagine`
--

LOCK TABLES `seq_immagine` WRITE;
/*!40000 ALTER TABLE `seq_immagine` DISABLE KEYS */;
/*!40000 ALTER TABLE `seq_immagine` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seq_indirizzo`
--

DROP TABLE IF EXISTS `seq_indirizzo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `seq_indirizzo` (
  `next_val` bigint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seq_indirizzo`
--

LOCK TABLES `seq_indirizzo` WRITE;
/*!40000 ALTER TABLE `seq_indirizzo` DISABLE KEYS */;
/*!40000 ALTER TABLE `seq_indirizzo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seq_lista_promo`
--

DROP TABLE IF EXISTS `seq_lista_promo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `seq_lista_promo` (
  `next_val` bigint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seq_lista_promo`
--

LOCK TABLES `seq_lista_promo` WRITE;
/*!40000 ALTER TABLE `seq_lista_promo` DISABLE KEYS */;
/*!40000 ALTER TABLE `seq_lista_promo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seq_marca`
--

DROP TABLE IF EXISTS `seq_marca`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `seq_marca` (
  `next_val` bigint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seq_marca`
--

LOCK TABLES `seq_marca` WRITE;
/*!40000 ALTER TABLE `seq_marca` DISABLE KEYS */;
/*!40000 ALTER TABLE `seq_marca` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seq_metodo_pagamento`
--

DROP TABLE IF EXISTS `seq_metodo_pagamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `seq_metodo_pagamento` (
  `next_val` bigint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seq_metodo_pagamento`
--

LOCK TABLES `seq_metodo_pagamento` WRITE;
/*!40000 ALTER TABLE `seq_metodo_pagamento` DISABLE KEYS */;
/*!40000 ALTER TABLE `seq_metodo_pagamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seq_ordine`
--

DROP TABLE IF EXISTS `seq_ordine`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `seq_ordine` (
  `next_val` bigint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seq_ordine`
--

LOCK TABLES `seq_ordine` WRITE;
/*!40000 ALTER TABLE `seq_ordine` DISABLE KEYS */;
/*!40000 ALTER TABLE `seq_ordine` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seq_pagamento`
--

DROP TABLE IF EXISTS `seq_pagamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `seq_pagamento` (
  `next_val` bigint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seq_pagamento`
--

LOCK TABLES `seq_pagamento` WRITE;
/*!40000 ALTER TABLE `seq_pagamento` DISABLE KEYS */;
/*!40000 ALTER TABLE `seq_pagamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seq_prodotto`
--

DROP TABLE IF EXISTS `seq_prodotto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `seq_prodotto` (
  `next_val` bigint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seq_prodotto`
--

LOCK TABLES `seq_prodotto` WRITE;
/*!40000 ALTER TABLE `seq_prodotto` DISABLE KEYS */;
/*!40000 ALTER TABLE `seq_prodotto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seq_promozione`
--

DROP TABLE IF EXISTS `seq_promozione`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `seq_promozione` (
  `next_val` bigint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seq_promozione`
--

LOCK TABLES `seq_promozione` WRITE;
/*!40000 ALTER TABLE `seq_promozione` DISABLE KEYS */;
/*!40000 ALTER TABLE `seq_promozione` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seq_ruolo`
--

DROP TABLE IF EXISTS `seq_ruolo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `seq_ruolo` (
  `next_val` bigint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seq_ruolo`
--

LOCK TABLES `seq_ruolo` WRITE;
/*!40000 ALTER TABLE `seq_ruolo` DISABLE KEYS */;
/*!40000 ALTER TABLE `seq_ruolo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seq_sede`
--

DROP TABLE IF EXISTS `seq_sede`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `seq_sede` (
  `next_val` bigint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seq_sede`
--

LOCK TABLES `seq_sede` WRITE;
/*!40000 ALTER TABLE `seq_sede` DISABLE KEYS */;
/*!40000 ALTER TABLE `seq_sede` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seq_shopping_list`
--

DROP TABLE IF EXISTS `seq_shopping_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `seq_shopping_list` (
  `next_val` bigint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seq_shopping_list`
--

LOCK TABLES `seq_shopping_list` WRITE;
/*!40000 ALTER TABLE `seq_shopping_list` DISABLE KEYS */;
/*!40000 ALTER TABLE `seq_shopping_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seq_spedizione`
--

DROP TABLE IF EXISTS `seq_spedizione`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `seq_spedizione` (
  `next_val` bigint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seq_spedizione`
--

LOCK TABLES `seq_spedizione` WRITE;
/*!40000 ALTER TABLE `seq_spedizione` DISABLE KEYS */;
/*!40000 ALTER TABLE `seq_spedizione` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seq_stato_ordine`
--

DROP TABLE IF EXISTS `seq_stato_ordine`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `seq_stato_ordine` (
  `next_val` bigint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seq_stato_ordine`
--

LOCK TABLES `seq_stato_ordine` WRITE;
/*!40000 ALTER TABLE `seq_stato_ordine` DISABLE KEYS */;
/*!40000 ALTER TABLE `seq_stato_ordine` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seq_tipi_pagamento`
--

DROP TABLE IF EXISTS `seq_tipi_pagamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `seq_tipi_pagamento` (
  `next_val` bigint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seq_tipi_pagamento`
--

LOCK TABLES `seq_tipi_pagamento` WRITE;
/*!40000 ALTER TABLE `seq_tipi_pagamento` DISABLE KEYS */;
/*!40000 ALTER TABLE `seq_tipi_pagamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seq_utente`
--

DROP TABLE IF EXISTS `seq_utente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `seq_utente` (
  `next_val` bigint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seq_utente`
--

LOCK TABLES `seq_utente` WRITE;
/*!40000 ALTER TABLE `seq_utente` DISABLE KEYS */;
/*!40000 ALTER TABLE `seq_utente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shopping_list`
--

DROP TABLE IF EXISTS `shopping_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `shopping_list` (
  `id_shopping_list` int NOT NULL,
  `quantità` int NOT NULL,
  `prodotto_id_prodotto` int DEFAULT NULL,
  `utente_id_utente` int DEFAULT NULL,
  `ordine_id_ordine` int DEFAULT NULL,
  PRIMARY KEY (`id_shopping_list`),
  KEY `FK6c8lqihyunsbq50qtwivj7i3p` (`prodotto_id_prodotto`),
  KEY `aaaa_idx` (`utente_id_utente`),
  KEY `FKhhyy9vup313xuani6sc7gaoid` (`ordine_id_ordine`),
  CONSTRAINT `aaaa` FOREIGN KEY (`utente_id_utente`) REFERENCES `utenti` (`id_Utente`),
  CONSTRAINT `FK6c8lqihyunsbq50qtwivj7i3p` FOREIGN KEY (`prodotto_id_prodotto`) REFERENCES `prodotti` (`id_Prodotto`),
  CONSTRAINT `FKhhyy9vup313xuani6sc7gaoid` FOREIGN KEY (`ordine_id_ordine`) REFERENCES `ordini` (`id_Ordine`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shopping_list`
--

LOCK TABLES `shopping_list` WRITE;
/*!40000 ALTER TABLE `shopping_list` DISABLE KEYS */;
/*!40000 ALTER TABLE `shopping_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spedizioni`
--

DROP TABLE IF EXISTS `spedizioni`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `spedizioni` (
  `id_Spedizione` int NOT NULL AUTO_INCREMENT,
  `data` date DEFAULT NULL,
  `tipo` varchar(45) DEFAULT NULL,
  `indirizzo_id_indirizzo` int DEFAULT NULL,
  `ordine_id_ordine` int DEFAULT NULL,
  PRIMARY KEY (`id_Spedizione`),
  KEY `FKdp2jpd1wjkb8kop38s53e6ylc` (`indirizzo_id_indirizzo`),
  KEY `FK3wq1ntj5n2bk501imqgx4huna` (`ordine_id_ordine`),
  CONSTRAINT `FK3wq1ntj5n2bk501imqgx4huna` FOREIGN KEY (`ordine_id_ordine`) REFERENCES `ordini` (`id_Ordine`),
  CONSTRAINT `FKdp2jpd1wjkb8kop38s53e6ylc` FOREIGN KEY (`indirizzo_id_indirizzo`) REFERENCES `indirizzi` (`id_Indirizzo`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spedizioni`
--

LOCK TABLES `spedizioni` WRITE;
/*!40000 ALTER TABLE `spedizioni` DISABLE KEYS */;
/*!40000 ALTER TABLE `spedizioni` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stato_ordini`
--

DROP TABLE IF EXISTS `stato_ordini`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `stato_ordini` (
  `id_stato_ordine` int NOT NULL,
  `descrizione` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_stato_ordine`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stato_ordini`
--

LOCK TABLES `stato_ordini` WRITE;
/*!40000 ALTER TABLE `stato_ordini` DISABLE KEYS */;
/*!40000 ALTER TABLE `stato_ordini` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipi_pagamenti`
--

DROP TABLE IF EXISTS `tipi_pagamenti`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tipi_pagamenti` (
  `id_tipo_pagamento` int NOT NULL,
  `cvv` int NOT NULL,
  `nome_titolare` varchar(255) DEFAULT NULL,
  `numero_carta` int NOT NULL,
  `scadenza` date DEFAULT NULL,
  `metodo_pagamento_id_metodo_pagamento` int DEFAULT NULL,
  PRIMARY KEY (`id_tipo_pagamento`),
  KEY `FK353i0w17dh63n5pi5ggo0ip0t` (`metodo_pagamento_id_metodo_pagamento`),
  CONSTRAINT `FK353i0w17dh63n5pi5ggo0ip0t` FOREIGN KEY (`metodo_pagamento_id_metodo_pagamento`) REFERENCES `metodi_pagamenti` (`id_metodo_pagamento`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipi_pagamenti`
--

LOCK TABLES `tipi_pagamenti` WRITE;
/*!40000 ALTER TABLE `tipi_pagamenti` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipi_pagamenti` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `utenti`
--

DROP TABLE IF EXISTS `utenti`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `utenti` (
  `id_Utente` int NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) DEFAULT NULL,
  `cognome` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `ruolo_id_ruolo` int DEFAULT '1',
  PRIMARY KEY (`id_Utente`),
  KEY `FK9amjr13m7nbmn589esb5x4j6h` (`ruolo_id_ruolo`),
  CONSTRAINT `FK9amjr13m7nbmn589esb5x4j6h` FOREIGN KEY (`ruolo_id_ruolo`) REFERENCES `ruoli` (`id_Ruolo`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `utenti`
--

LOCK TABLES `utenti` WRITE;
/*!40000 ALTER TABLE `utenti` DISABLE KEYS */;
INSERT INTO `utenti` VALUES (1,'Matteo','Ciano','matteociano@kitcheninlove.it','02345601','mciano','matteoCi',2);
/*!40000 ALTER TABLE `utenti` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-02-15 16:28:10
