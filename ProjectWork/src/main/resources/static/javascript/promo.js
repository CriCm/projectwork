	var obj = [];
	var idPromozione = -1;
		
//Per le spiegazioni dei vari metodi vedere giacenza.js
//questo non invoca il selezionaSede (chiamato val in giacenza.js) sull'onChange, ma sull'onload e prende l'id dalla promo selezionata nel link precedente
//differisce inoltre per la presenza di un check al posto della modifica della quantità
		
	function getObj(method,url, params) {
		var xmlHttp = new XMLHttpRequest();
		xmlHttp.overrideMimeType("application/json");
		xmlHttp.open(method, url , false); 
		xmlHttp.send(params);
		const json = xmlHttp.responseText;
		if (json.length>0)
			 obj = JSON.parse(json);
	    else
	         obj=[];
		return obj;
	}

	function push(idProdotto){
		if(document.getElementById("check"+idProdotto).checked)
  			obj.push({"idProdotto": idProdotto, "idPromozione" : idPromozione}); 
	}


	function cerca(){
		var classe = document.getElementsByClassName("smaal");
	  	for(i=0;i<classe.length;i++){
	  		var trovato = false;
	  		var str = classe[i].id;
			var res = parseInt(str.substring(5));	
			for(j=0; j<obj.length; j++){
				if(obj[j] != null && obj[j].idProdotto == res){
					trovato = true;
	  			    break;		
	  			}
	  		}
	  		if(trovato == false)
	  			push(res);
	  	}	
	}
	
  	function reset() {
	var classe = document.getElementsByClassName("smaal");
	for(i=0;i<classe.length;i++)
	  		document.getElementById(classe[i].id).checked = false;
	}
  
	function salva(){
  		cerca();
  		var jsonStr = JSON.stringify(obj);
  		d = document.getElementById("idPromozione").value;
  		getObj("POST", "http://localhost:8080/Admin/ListaPromo/"+ d, jsonStr);
  		alert(obj["status"]);
  		obj=[];
  		selezionaSede();
	}
	
	function selezionaSede() {
	  	obj=[];
	  	reset();
	    d = document.getElementById("idPromozione").value;
		idPromozione = d;
		var temp = getObj("GET","http://localhost:8080/Admin/ListaPromo/"+ d, null);
		obj = obj.concat(temp);
		for(i = 0; i < obj.length; i++){
			if(obj[i].idProdotto != null)
				document.getElementById('check' + obj[i].idProdotto).checked= true;
		}
		var classe = document.getElementsByClassName("smaal");
		
		for(i=0;i<classe.length;i++){
			var text = document.getElementById("text"+classe[i].id);
			if(document.getElementById(classe[i].id).checked == true){
				text.style.display = "block";
			}
			else {
    			text.style.display = "none";
  			}	
	  	}
	}