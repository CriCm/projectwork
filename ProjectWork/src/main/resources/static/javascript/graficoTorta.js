function graficoTorta(id, obj, colori){
var ctx = document.getElementById(id);

  var torta = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: obj.nomeProdotto,
      
      datasets: [{
        data: obj.quantitaProdotto,
        backgroundColor: colori,
        borderWidth: 0,
      }]
    },
    options: {
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true
          }
        }]
      }
    }
  })
}