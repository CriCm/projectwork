function graficoBarreObj(id, asseX, asseY, colori){
var ctx = document.getElementById(id);

var graficoBarre = new Chart(ctx, {
    type: 'bar',
    data: {
      labels: asseX,    
      datasets: [{
        data: asseY,
        lineTension: 0,
        backgroundColor: colori,
        borderWidth: 0,
        pointBackgroundColor: '#E67E22'
      }]
    },
    options: {
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true
          }
        }]
      },
      legend: {
        display: false
      }
    }
  })
}