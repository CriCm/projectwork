	var obj = [];
	var idSede = -1;
	
/*
-XMLHttpRequest è una API (interfaccia alla progammazione) la quale fornisce al client le funzionalità di trasferire 
	bidirezionalmente dati tra esso ed il server in maniera sincrona od asincrona 
	senza che il browser richieda al server una porzione di dati senza necessariamente effettuar l'aggiornamento della pagina.
-XMLHttpRequest.open() inizializza una chiamata;
-XMLHttpRequest.send() invia la richiesta. Se la richiesta è asincrona (come è di default) questo metodo 
	risponde non appena terminato l'invio della richiesta attiva.
-Il metodo xmlHttp.responseText restituisce una stringa che contiene l'esito della chiamata;
-Il metodo JSON.parse() analizza una stringa JSON, costruendo il valore JavaScript o l'oggetto descritto dalla stringa.
*/	
	function getObj(method,url, params) {
		var xmlHttp = new XMLHttpRequest();
		xmlHttp.overrideMimeType("application/json");
		xmlHttp.open(method, url , false); 
		xmlHttp.send(params);
		const json = xmlHttp.responseText;
		if (json.length>0)
			 obj = JSON.parse(json);
	    else
	         obj=[];
		return obj;
	}

/*
-Fa un push su obj di un oggetto composto dall'id del prodotto che gli passiamo,
	la quantità che viene inserita sull'elemento con id "qty"+idProdotto(se maggiore di 0) e la sede selezionata
*/ 
	function push(idProdotto){
		var qty = parseInt(document.getElementById("qty"+idProdotto).value);
		if(qty>0)
  			obj.push({"id": idProdotto, "idSede" : idSede, "quantita" : qty}); 
	}

/*
-Per ogni elemento con classe "smaal"
-Assegna alla variabile trovato valore false;
-Prende ogni id di questa classe (es: "qty1");
-Assegna a res tutto ciò che viene dopo il terzo carattere di questa stringa;
-Controlla ogni elemento di obj: 
	-se è diverso da null e corrisponde a res, assegna a trovato valore true e,
		se il valore inserito nel campo con id "qty"+obj[j] è maggiore di 0, somma questo valore alla quantità di obj
	-altrimenti invoca la funzione push() passandogli come paramentro res (l'id del prodotto che cerchiamo).
*/
  
	function cerca(){
		var classe = document.getElementsByClassName("smaal");
	  	for(i=0;i<classe.length;i++){
	  		var trovato = false;
	  		var str = classe[i].id;
			var res = parseInt(str.substring(3));	
			for(j=0; j<obj.length; j++){
				if(obj[j] != null && obj[j].id == res){
					trovato = true;
					if (parseInt(document.getElementById("qty"+obj[j].id).value) > 0)
	  					obj[j].quantita = obj[j].quantita + parseInt(document.getElementById("qty"+obj[j].id).value);
	  			    break;		
	  			}
	  		}
	  		if(trovato == false)
	  			push(res);
	  	}	
	}
	
	function reset() {
	var classe = document.getElementsByClassName("smaal");
	for(i=0;i<classe.length;i++)
	  		document.getElementById(classe[i].id).value=0;
	}
	
/* 
-Richiama la funzione cerca();
-Converte l'oggetto JavaScript in una stringa JSON
-Richiama il metodo getObj a cui passa metodo "post", url con idSede, la stringa json;
(Vedere @PostMapping in GiacenzaRestController)
*/  
	function salva(){
  		cerca();
  		var jsonStr = JSON.stringify(obj);
  		d = document.getElementById("select_id").value;
  		getObj("POST", "http://localhost:8080/Admin/Giacenza/"+ d, jsonStr);
  		obj=[];
  		document.getElementById("Sede"+d).selected="true";
  		val();
  		reset();
	}
	
/*
metodo che si potrebbe chiamare anche "selezionaSede". Viene invocato nel momento in cui si effettua un "onchange" 
sul menù a tendina in cui vengono visualizzati i nomi delle sedi. 
-Resetta obj(necessario perchè altrimenti se io clicco per sbaglio su una sede e poi su un' altra vengono concatenati più elementi)
-Assegna a d il valore dell'elemento con id "select_id".
-Assegna a idSede il valore di d;
-Richiama il metodo getObj con metodo "Get", url con accodato l'id della sede, nessun paramentro;
-Fa un push su obj di questo nuovo obj generato(temp);
-Per ogni elemento presente in obj assegna all'elemento con id "giacenza..." la quantità presente in obj
*/
	function val() {
	  	obj=[];
	    d = document.getElementById("select_id").value;
		idSede = d;
		var temp = getObj("GET","http://localhost:8080/Admin/Giacenza/"+ d, null);
		obj = obj.concat(temp);
		for(i = 0; i < obj.length; i++){
			if(obj[i].quantita != null)
				document.getElementById('giacenza' + obj[i].id).innerHTML = obj[i].quantita;
		}
	}