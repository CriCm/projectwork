function caricamento(){

	var indirizzo = "http://localhost:8080/Admin/ReportFatturato";
	var oggetto = getObj(indirizzo);
	var asseX= oggetto.nomeMese;
	var asseY= oggetto.fatturato;
	graficoBarreObj("totaleVendite", asseX, asseY, ["#87cefa","#20b2aa","#66cdaa", "#9370db","#ba55d3","#dda0dd", "#b0e0e6","#40e0d0", "#12ffcf", "#ffcd12", "#bcf819" , "#b65bb4"]);


	var indirizzo1 = "http://localhost:8080/Admin/reportPiuVenduti/";
	var oggetto1 = getObj(indirizzo1);
	var asseX1= oggetto1.nomeProdotto;
	var asseY1= oggetto1.quantitaProdotto;
	graficoBarreObj("piuVenduti", asseX1, asseY1, ['#92f200','#adff2f','#cbff7bE','#ccff00','#edff21']);
	
	
	var oggetto2 = getObj("http://localhost:8080/Admin/reportMenoVenduti/");    //modularizzato (refactorizzato)
	graficoBarreObj("menoVenduti", oggetto2.nomeProdotto, oggetto2.quantitaProdotto, ['#E71919','#fde910','#f8f32b','#ffff66','#edff21']);

	
	var url = "http://localhost:8080/Admin/ReportSLLite";
	var obj = getObj(url);
	var colori = new Array();
	for(var i = 0; i < obj.nomeProdotto.length; i++)
    colori.push(getRandomColor());
	graficoTorta("torta", obj , colori);


}