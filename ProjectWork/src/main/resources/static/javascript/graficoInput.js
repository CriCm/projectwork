function requestReport(){
      
      	var numMese = document.getElementById("mese").value;
      	var numAnno = document.getElementById("anno").value;
      	var numMese2 = document.getElementById("mese2").value;
      	var numAnno2 = document.getElementById("anno2").value;
      	var urlReq = "http://localhost:8080/Admin/ReportPerData?mese=" + numMese + "&anno=" + numAnno + "&mese2=" + numMese2 + "&anno2=" + numAnno2;
      	//alert(urlReq);
      	var oReq = new XMLHttpRequest();
      	oReq.open("GET", urlReq, false);
      	oReq.send();
      	const json = oReq.responseText;
      	const obj = JSON.parse(json);
      	//alert(json);
      	

  var ctx = document.getElementById('reportPerData')
  var reportPerData = new Chart(ctx, {
    type: 'bar',
    data: {
      labels: [1, 2],
      datasets: [{
        data: obj.fatturato
        ,
        lineTension: 0,
        backgroundColor: ["#66cdaa", "#9370db"],
        borderWidth: 0,
        pointBackgroundColor: '#E67E22'
      }]
    },
    options: {
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true
          }
        }]
      },
      legend: {
        display: false
      }
    }
  })
      	
      	}