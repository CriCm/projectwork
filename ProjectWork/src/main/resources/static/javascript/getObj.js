function getObj(url) {
	var xmlHttp = new XMLHttpRequest();
	xmlHttp.open("GET", url , false );
	xmlHttp.send(null);
	const json = xmlHttp.responseText;
	const obj = JSON.parse(json);
    return obj;
  }
