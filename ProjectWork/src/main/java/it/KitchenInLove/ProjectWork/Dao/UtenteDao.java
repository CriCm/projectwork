package it.KitchenInLove.ProjectWork.Dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import it.KitchenInLove.ProjectWork.Model.Utente;

public interface UtenteDao extends CrudRepository <Utente, Integer> {
	Utente findByIdUtente (int IdUtente);
	
	//RESTITUISCE UN UTENTE IL CUI USERNAME E PASSWORD SIANO QUELLI SPECIFICATI
	@Query("select s from Utente s where username= :aliasInserito and password = :passwordInserita")
	public Utente login(String aliasInserito, String passwordInserita);
	

}