package it.KitchenInLove.ProjectWork.Dao;
import org.springframework.data.repository.CrudRepository;
import it.KitchenInLove.ProjectWork.Model.Ordine;

public interface OrdineDao extends CrudRepository <Ordine, Integer> {
	Ordine findByIdOrdine (int idOrdine);
}
