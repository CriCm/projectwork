package it.KitchenInLove.ProjectWork.Dao;
import org.springframework.data.repository.CrudRepository;

import it.KitchenInLove.ProjectWork.Model.StatoOrdine;

public interface StatoOrdineDao extends CrudRepository <StatoOrdine, Integer> {
	StatoOrdine findByIdStatoOrdine (int idStatoOrdine);
}
