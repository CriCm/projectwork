package it.KitchenInLove.ProjectWork.Dao;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import it.KitchenInLove.ProjectWork.Model.Indirizzo;

public interface IndirizzoDao extends CrudRepository <Indirizzo, Integer> {
	Indirizzo findById(int id);
	
	//RESTITUISCE LA LISTA DEGLI INDIRIZZI DI UN DETERMINATO UTENTE
	@Query("select i from Indirizzo i join i.utente u where u.idUtente = :idUtente")
	List <Indirizzo> cercaPerIdUtente (int idUtente);
}