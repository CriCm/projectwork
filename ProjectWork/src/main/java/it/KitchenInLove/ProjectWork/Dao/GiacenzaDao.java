package it.KitchenInLove.ProjectWork.Dao;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import it.KitchenInLove.ProjectWork.Model.Giacenza;


public interface GiacenzaDao extends CrudRepository <Giacenza, Integer>{
	Giacenza findByIdGiacenza(int idGiacenza);
	
	//RESTITUISCE UNA LISTA DI PRODOTTI CON RELATIVA QUANTITÀ IN UNA DATA SEDE
	//Select p.nome, g.quantità from Giacenza as g join prodotti as p on g.prodotto_id_prodotto = p.id_Prodotto where g.sede_id_sede = 1
	@Query ("Select g from Giacenza g join g.sede s where s.idSede = :idSede")
	List <Giacenza> giacenzaInSede(int idSede);
	
	//RESTITUISCE UN DETERMINATO PRODOTTO IN UNA DETERMINATA SEDE CON LA RELATIVA QUANTITà
	@Query ("Select g from Giacenza g join g.prodotto p join g.sede s where s.idSede = :idSede and p.idProdotto = :idProdotto")
	Giacenza getSingolaGiacenza(int idProdotto, int idSede);
	
	//RESTITUISCE LA QUANTITà DI UN DETERMINATO PRODOTTO IN UNA DETERMINATA SEDE
	@Query("Select g.quantita from Giacenza g join g.prodotto p join g.sede s where p.idProdotto = :idProdotto and s.idSede = :idSede")
	Integer getQuantita(int idProdotto,int idSede);
			
}
