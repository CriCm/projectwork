package it.KitchenInLove.ProjectWork.Dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import it.KitchenInLove.ProjectWork.Model.Categoria;

public interface CategoriaDao extends CrudRepository <Categoria, Integer> {
	Categoria findByIdCategoria (int id);
	
	//LISTA DI TUTTE LE CATEGORIE DEFINITE PRINCIPALI (QUELLE CHE NON PRESENTANO UN IDGENITORE(=0))
	@Query("select c from Categoria c where c.idGenitore = 0")
	List <Categoria> categoriePrincipali();
	
	//LISTA DI TUTTE LE SOTTOCATEGORIE (TUTTE QUELLE CHE HANNO UN IDGENITORE, CHE È L'ID DELLA CATEGORIA PRINCIPALE) 
	@Query("select c from Categoria c where c.idGenitore = :idGenitore")
	List <Categoria> sottoCategoria(int idGenitore);
}