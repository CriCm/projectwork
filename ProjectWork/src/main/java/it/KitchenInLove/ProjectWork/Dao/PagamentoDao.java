package it.KitchenInLove.ProjectWork.Dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import it.KitchenInLove.ProjectWork.Model.Pagamento;

public interface PagamentoDao extends CrudRepository<Pagamento, Integer>{
	Pagamento findByIdPagamento(int idPagamento);
	
	//RESTITUISCE IL FATTURATO TOTALE (SOMMA DEI PAGAMENTI) IN UN DETERMINATO MESE ED ANNO
	@Query("Select sum(pa.importo) from Pagamento pa where month(pa.dataPagamento) = :mese and year(pa.dataPagamento)= :anno") 
	Double fatturatoSemplice(int mese, int anno);
}

