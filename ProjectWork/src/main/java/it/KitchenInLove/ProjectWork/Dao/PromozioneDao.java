package it.KitchenInLove.ProjectWork.Dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import it.KitchenInLove.ProjectWork.Model.Promozione;

public interface PromozioneDao extends CrudRepository <Promozione, Integer> {
	Promozione findByIdPromozione(int idPromozione);
	Promozione findByIdPromozione (Optional<Integer> idPromo);
	List <Promozione> findAll();

	//RESTITUISCE LE PROMOZIONI NON ANCORA FINITE E CHE INIZINO FINO AL MESE PROSSIMO 
	//(es. una promo iniziata un mese fa che finisce domani, ma anche una promo che inizia tra una settimana e finisce tra 2 mesi) 
    @Query("select p from Promozione p where dataFine > curdate() and dataInizio < curdate() +30")
    public List<Promozione> promozioniCorrenti();
    
    //RESTITUISCE UNA LISTA DI PROMOZIONI GIà INIZIATE E NON ANCORA FINITE
    @Query("select p from Promozione p where dataFine > curdate() and dataInizio < curdate()")
    List<Promozione> promozioneAttuale();
    
    //RESTITUISCE UNA LISTA DI PROMOZIONI AL CUI INTERNO SI TROVA UN DETERMINATO PRODOTTO
    @Query("Select pr from Promozione pr join pr.listaPromozioni lp join lp.prodotto p where pr.dataFine > curdate() and p.idProdotto = :idProdotto")
	List <Promozione> cercaProdInPromoCorr(int idProdotto);
}
