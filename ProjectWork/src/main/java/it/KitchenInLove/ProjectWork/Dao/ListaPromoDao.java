package it.KitchenInLove.ProjectWork.Dao;

import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import it.KitchenInLove.ProjectWork.Model.ListaPromo;
import it.KitchenInLove.ProjectWork.Model.Prodotto;

public interface ListaPromoDao extends CrudRepository <ListaPromo, Integer> {
	ListaPromo findByIdListaPromo(int idListaPromo);
	
	//RESTITUISCE UNA LISTA DI PRODOTTI CHE SONO ALL'INTERNO DI UNA DETERMINATA PROMOZIONE
	@Query("select p from Prodotto p join p.listaPromozioni lp join lp.promozione pr where pr.idPromozione = :idPromo")
	List <Prodotto> cercaPerIdPromo(Optional<Integer> idPromo);
	
	//RESTITUISCE UNA SINGOLA LISTA PROMO(RELAZIONE) CON UN DETERMINATO PRODOTTO ED UNA DETERMINATA PROMOZIONE 
	@Query ("Select l from ListaPromo l join l.prodotto p join l.promozione pr where pr.idPromozione = :idPromozione and p.idProdotto = :idProdotto")
	ListaPromo getSingolaListaPromo(int idProdotto, int idPromozione);
	
	//RESTITUISCE UNA LISTA DI LISTEPROMO(RELAZIONI) CON I PRODOTTI IN UNA DETERMINATA PROMOZIONE ()
	@Query ("Select lp from ListaPromo lp join lp.promozione p where p.idPromozione = :idPromozione")
	List <ListaPromo> listaPerPromozione(int idPromozione);
}
