package it.KitchenInLove.ProjectWork.Dao;

import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import it.KitchenInLove.ProjectWork.Model.Ordine;
import it.KitchenInLove.ProjectWork.Model.Prodotto;
import it.KitchenInLove.ProjectWork.Model.ShoppingList;
import it.KitchenInLove.ProjectWork.Model.Utente;


public interface ShoppingListDao extends CrudRepository<ShoppingList, Integer> {
	    ShoppingList findByIdShoppingList(int idShoppingList);
	    List <ShoppingList> findByOrdine(Ordine ordine); 	
	    
	    //RESTITUISCE UNA LISTA DI PRODOTTI(LA CUI AMPIEZZA VIENE DEFINITA DALL'ATTRIBUTO PAGEABLE) PIù VENDUTI ORDINATI IN ORDINE DISCENDENTE 
	    /*select * from prodotti as p JOIN shopping_list as sl on sl.prodotto_id_prodotto = p.id_Prodotto JOIN ordini as o on sl.ordine_id_ordine = o.id_Ordine JOIN stato_ordini as so on so.id_stato_ordine = o.stato_ordine_id_stato_ordine
	    where so.id_stato_ordine = 3 group by p.id_Prodotto order by sl.quantità desc;*/
	    @Query ("Select p from Prodotto p JOIN p.listaShoppingList lsl JOIN lsl.ordine o JOIN o.statoOrdine so where so.idStatoOrdine != 4 group by p.idProdotto order by lsl.quantita desc")
	    List <Prodotto> piuVenduti(Pageable pageable);
	    
	    //RESTITUISCE UNA LISTA DI PRODOTTI(LA CUI AMPIEZZA VIENE DEFINITA DALL'ATTRIBUTO PAGEABLE) MENO VENDUTI ORDINATI IN ORDINE DISCENDENTE
	    @Query ("Select p from Prodotto p JOIN p.listaShoppingList lsl JOIN lsl.ordine o JOIN o.statoOrdine so where so.idStatoOrdine != 4 group by p.idProdotto order by lsl.quantita asc")
	    List <Prodotto> menoVenduti(Pageable pageable);
	    
	    //RESTITUISCE UNA LISTA DI SHOPPINGLIST PER CUI è STATO EFFETTUATO L'ORDINE(NON PIù NEL CARRELLO)
	    @Query ("Select sl from ShoppingList sl where sl.ordine != null")
	    List <ShoppingList> getOrdiniEffettuati();
	    
	    //RESTITUISCE LA LISTA DEI PRODOTTI ACQUISTATI DAL CLIENTE E IL NUMERO DI ORDINE ASSOCIATO
        @Query ("Select sl from ShoppingList sl JOIN sl.utente u JOIN sl.ordine o where u.idUtente = :idUtente group by o.idOrdine order by o.idOrdine ")
        List <ShoppingList> listaOrdiniGroup(int idUtente);
        
        //RESTITUISCE LA LISTA DEI PRODOTTI ATTUALMENTE NEL CARRELLO DELL'UTENTE SPECIFICATO
        @Query ("Select s from ShoppingList s JOIN s.utente u where s.ordine = null and u.idUtente = :idUtente")
	    List <ShoppingList> getListShoppingList(int idUtente);
        
        //RESTITUISCE LA QUANTITà DI UN DETERMINATO PRODOTTO INSERITO NEL CARRELLO DEL CLIENTE SPECIFICATO (potevamo far restituire direttamente Integer quantita)
        @Query ("Select s from ShoppingList s JOIN s.utente u JOIN s.prodotto p where s.ordine = null and u.idUtente = :idUtente and p.idProdotto = :idProdotto")
        ShoppingList getSingolaShoppingList (int idUtente, int idProdotto);
        
        //RESTITUISCE LA LISTA DEI NOMI DEI PRODOTTI ACQUISTATI (da visualizzare sul grafico)
        @Query("Select sl from ShoppingList sl join sl.prodotto p where sl.ordine != null group by p.idProdotto")
    	List <ShoppingList> nomiProdotto();
        
        //RESTITUISCE LA LISTA DELLE QUANTITà DEI PRODOTTI ACQUISTATI(da visualizzare sul grafico)
    	@Query("Select sum(sl.quantita) from ShoppingList sl join sl.prodotto p where sl.ordine != null group by p.idProdotto")
    	List<Integer> quantitaProdotto();
    	
    	//RESTITUISCE LA LISTA DELLE QUANTITà DEI PRODOTTI ACQUISTATI (LA CUI AMPIEZZA VIENE DEFINITA DALL'ATTRIBUTO PAGEABLE) IN ORDINE DECRESCENTE
    	@Query("Select sum(sl.quantita) as su from ShoppingList sl join sl.prodotto p where sl.ordine != null group by p.idProdotto order by su desc")
    	List<Integer> quantitaProdottoDesc(Pageable pageable);
    	
    	//RESTITUISCE LA LISTA DELLE QUANTITà DEI PRODOTTI ACQUISTATI (LA CUI AMPIEZZA VIENE DEFINITA DALL'ATTRIBUTO PAGEABLE) IN ORDINE ASCENDENTE
    	@Query("Select sum(sl.quantita) as su from ShoppingList sl join sl.prodotto p where sl.ordine != null group by p.idProdotto order by su asc")
    	List<Integer> quantitaProdottoAsc(Pageable pageable);
    	
    	//RESTITUISCE LA DIFFERENZA TRA LA QUANTITà IN GIACENZA DI UN DETERMINATO PRODOTTO E LA QUANTITà INSERITA NEL CARRELLO DI UN DETERMINATO CLIENTE
    	/*select g.quantità - sl.quantità from shopping_list as sl join prodotti as p on p.id_Prodotto = sl.prodotto_id_prodotto 
		join giacenza as g on g.prodotto_id_prodotto = p.id_prodotto
		where sl.utente_id_utente = 1 and sl.ordine_id_ordine is null and g.sede_id_sede = 1 and p.id_Prodotto = 4;*/
    	@Query("Select g.quantita - sl.quantita from Giacenza g join g.prodotto p join p.listaShoppingList sl join g.sede s where sl.utente = :utente and sl.ordine = null and s.idSede = :idSede and p.idProdotto = :idProdotto")
    	Integer quantita(Utente utente, int idProdotto, int idSede);
    	
}


