package it.KitchenInLove.ProjectWork.Dao;

import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import it.KitchenInLove.ProjectWork.Model.Prodotto;

public interface ProdottoDao extends CrudRepository <Prodotto, Integer> {
	List <Prodotto> findAll();
	List <Prodotto> findByCategoriaIdCategoria (Optional<Integer> idCategoria);
	Prodotto findByIdProdotto (int IdProdotto);
	
	//LISTA DEI PRODOTTI FILTRATA TRAMITE PARTE DEL NOME O DELLA BREVE DESCRIZIONE
	//select p.nome, p.breve_Descrizione from prodotti as p where p.nome like "%chi%";
	@Query("select p from Prodotto p where p.nome || p.breveDescrizione like %:stringa%")
	List <Prodotto> cerca(String stringa);
	
	//RESTITUISCE LA LISTA DEI PRODOTTI INSERITI NEL CARRELLO DI UN DETERMINATO UTENTE
	@Query ("Select p from Prodotto p JOIN p.listaShoppingList lsl JOIN lsl.utente u JOIN lsl.ordine o where o.idOrdine = :idOrdine and u.idUtente = :idUtente")
	List <Prodotto> getProdottiShoppingList(int idUtente, int idOrdine);
	
	//RESTITUISCE UNA LISTA DI PRODOTTI IN UNA DETERMINATA SEDE
	@Query("select p from Prodotto p JOIN p.listaGiacenza lg JOIN lg.sede s Where s.idSede = :idSede")
	List <Prodotto> prodottiInGiacenza(int idSede);
	
	//RESTITUISCE LA LISTA DEI PRODOTTI MENO VENDUTI
	@Query ("Select p from Prodotto p JOIN p.listaShoppingList lsl JOIN lsl.ordine o JOIN o.statoOrdine so where so.idStatoOrdine != 4 group by p.idProdotto order by lsl.quantita asc")
    List <Prodotto> menoVendutiDaMostrareSuHome();
}




