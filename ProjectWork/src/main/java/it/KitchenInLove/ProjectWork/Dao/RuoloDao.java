package it.KitchenInLove.ProjectWork.Dao;
import org.springframework.data.repository.CrudRepository;
import it.KitchenInLove.ProjectWork.Model.Ruolo;

public interface RuoloDao extends CrudRepository <Ruolo, Integer> {
	Ruolo findByIdRuolo (int idRuolo);
}
