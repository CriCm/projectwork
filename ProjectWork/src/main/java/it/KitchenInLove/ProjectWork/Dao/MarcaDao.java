package it.KitchenInLove.ProjectWork.Dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import it.KitchenInLove.ProjectWork.Model.Marca;
import it.KitchenInLove.ProjectWork.Model.Prodotto;

public interface MarcaDao extends CrudRepository<Marca, Integer> {
    Marca findByIdMarca(int id);
    
    //RESTITUISCE UNA LISTA DI MARCHE IDENTIFICATE TRAMITE ATTRIBUTO GRANDE
    @Query ("select m from Marca m where m.grande = 1")
    List <Marca> grandiMarche();
    
    //RESTITUISCE UNA LISTA DI PRODOTTI CHE HANNO UNA DETERMINATA MARCA
    @Query ("select p from Prodotto p JOIN p.marca m where m.idMarca = :idMarca")
    List <Prodotto> prodMarca (Optional<Integer> idMarca);
}
