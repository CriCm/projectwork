package it.KitchenInLove.ProjectWork.Dao;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import it.KitchenInLove.ProjectWork.Model.Sede;

public interface SedeDao extends CrudRepository <Sede, Integer> {
	List <Sede> findAll();
	Sede findByIdSede (int idSede);
	
}
