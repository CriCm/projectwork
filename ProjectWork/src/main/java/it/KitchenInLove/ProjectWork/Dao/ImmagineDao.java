package it.KitchenInLove.ProjectWork.Dao;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import it.KitchenInLove.ProjectWork.Model.Immagine;

public interface ImmagineDao extends CrudRepository <Immagine, Integer> {
	Immagine findByIdImmagine (int idImmagine);
	
	//RESTITUISCE TUTTE LE IMMAGINI DI UN DETERMINATO PRODOTTO
	@Query("select i from Immagine i join i.prodotto p where p.idProdotto = :idProdotto")
	List <Immagine> cercaPerIdProdotto(int idProdotto);
	
	//RESTITUISCE LE IMMAGINI DEFINITE PRINCIPALI DI UN DETERMINATO PRODOTTO
	@Query("select i from Immagine i join i.prodotto p where p.idProdotto = :idProdotto and i.principale=1")
	List <Immagine> cercaPerIdProdottoImmPrincipale(int idProdotto);
	
	//RESTITUISCE TUTTE LE IMMAGINI DI UN PRODOTTO CHE NON SONO STATE DEFINITE COME PRINCIPALI
	@Query("select i from Immagine i join i.prodotto p where p.idProdotto = :idProdotto and i.principale=0")
	List <Immagine> cercaPerIdProdottoImmSecondarie(int idProdotto);
}
