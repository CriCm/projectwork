package it.KitchenInLove.ProjectWork.Controller;

import java.time.LocalDate;
import java.util.List;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import it.KitchenInLove.ProjectWork.Dao.GiacenzaDao;
import it.KitchenInLove.ProjectWork.Dao.IndirizzoDao;
import it.KitchenInLove.ProjectWork.Dao.OrdineDao;
import it.KitchenInLove.ProjectWork.Dao.PagamentoDao;
import it.KitchenInLove.ProjectWork.Dao.ProdottoDao;
import it.KitchenInLove.ProjectWork.Dao.RuoloDao;
import it.KitchenInLove.ProjectWork.Dao.ShoppingListDao;
import it.KitchenInLove.ProjectWork.Dao.StatoOrdineDao;
import it.KitchenInLove.ProjectWork.Dao.UtenteDao;
import it.KitchenInLove.ProjectWork.Model.Giacenza;
import it.KitchenInLove.ProjectWork.Model.Indirizzo;
import it.KitchenInLove.ProjectWork.Model.Ordine;
import it.KitchenInLove.ProjectWork.Model.Pagamento;
import it.KitchenInLove.ProjectWork.Model.Prodotto;
import it.KitchenInLove.ProjectWork.Model.Ruolo;
import it.KitchenInLove.ProjectWork.Model.Sede;
import it.KitchenInLove.ProjectWork.Model.ShoppingList;
import it.KitchenInLove.ProjectWork.Model.StatoOrdine;
import it.KitchenInLove.ProjectWork.Model.Utente;

@Controller
@RequestMapping(path="/Utente")
public class UtenteController {
	
	@Autowired
	UtenteDao utenteRepository;
	@Autowired
	ShoppingListDao shoppingListRepository;
	@Autowired
	IndirizzoDao indirizzoRepository;
	@Autowired
	ProdottoDao prodottoRepository;
	@Autowired
	OrdineDao ordineRepository;
	@Autowired
	StatoOrdineDao statoOrdineRepository;
	@Autowired
	RuoloDao ruoloRepository;
	@Autowired
	PagamentoDao pagamentoRepository;
	@Autowired
	GiacenzaDao giacenzaRepository;
	
	@GetMapping("/")
	public String index() {
		return "Login";
	}
	
	//ACCOUNT UTENTE/PROFILO
	@GetMapping("/Profilo") 
	public String profilo (Model model, HttpSession session) {
		Utente utente = (Utente) session.getAttribute("utenteAutenticato");
		
		if (utente != null) {
			List <Indirizzo> listaIndirizzi = indirizzoRepository.cercaPerIdUtente(utente.getIdUtente());
			List <ShoppingList> shoppingListGroup = shoppingListRepository.listaOrdiniGroup(utente.getIdUtente());
		model.addAttribute("utente", utente);
		model.addAttribute("PHIndirizzi", listaIndirizzi);
		model.addAttribute("shoppingListGroup", shoppingListGroup);
		
		return "AccountUtente";
		}
		return "redirect:/";
	} 	
	
	//MODIFICA PROFILO 
	//metodo che restituisce ModelAndView, si è deciso di non aggiornarlo per avere un esempio di alternativa
	@GetMapping("/Profilo/Modifica/{id}")
	public ModelAndView modifica(@PathVariable("id") int id) {
		Utente utente = utenteRepository.findByIdUtente(id);
        ModelAndView modelAndView = new ModelAndView();

        if (utente != null){
            modelAndView.setViewName("ModificaUtente");
            modelAndView.addObject("utente", utente);
            return modelAndView;
        }else {
            return null;
        }
	}
	
	@PostMapping("/Profilo/Modifica")
	public String postModifica(@Valid Utente utente, BindingResult bindingResult, Model model, HttpSession session) {
		if (bindingResult.hasErrors()) {
			model.addAttribute("utente", utente);
			return "ModificaUtente";
		}
		session.setAttribute("utenteAutenticato", utente);
		utenteRepository.save(utente);
        return "redirect:/Utente/Profilo";
	}
	
	
	//MODIFICA PASSWORD
	@GetMapping("/Profilo/Modifica/Password/{id}")
	public String modificaPassword(@PathVariable("id") int id, Model model) {	
		Utente utente = utenteRepository.findByIdUtente(id);
		model.addAttribute("utente", utente);
            return "ModificaPassword";
	}
	
	@PostMapping("/Profilo/Modifica/Password")
	public String postModificaPassword(@Valid Utente utente, @RequestParam("password") String password, @RequestParam("confPassword") String confPassword, BindingResult bindingResult, Model model) {
		if (bindingResult.hasErrors()) {
			model.addAttribute("utente", utente);
			return "ModificaPassword";
		}
		if(password.equals(confPassword))
		utenteRepository.save(utente);
		else
			return "ModificaPassword";
        return "redirect:/Utente/Profilo";
	}

	
	//LOGIN-AUTENTICAZIONE
	@GetMapping("/Login")
	public String login() {
		return "Login";
	}
	
	@PostMapping("/Login")
	public String postLogin(@RequestParam("username") String username, @RequestParam("password") String password, HttpSession session, Model model) { 
		Utente utente = utenteRepository.login(username, password);
		if (utente == null) {
		    model.addAttribute("msgErr", "Username o password non corretti");
		    return "Login";
		}
		else { 
			Ruolo ruolo = utente.getRuolo();
			if ((ruolo.getIdRuolo() == 1)) {
			    session.setAttribute("utenteAutenticato", utente);			
				return "redirect:/";
				}
			
		    if ((ruolo.getIdRuolo() == 2)) {
			    session.setAttribute("utenteAutenticato", utente);			
				return "redirect:/Admin/";
				}
		    if ((ruolo.getIdRuolo() == 3)) {
			    session.setAttribute("utenteAutenticato", utente);			
				return "redirect:/Giacenza/";
				}
			}
        return "redirect:/Login";					
		}
	
	
	//LOGOUT
	@RequestMapping("/Logout")
	public String logout(HttpSession session) {
		session.setAttribute("utenteAutenticato", null);
		return "redirect:/";
	}
	
	
	//REGISTRAZIONE
	@GetMapping("/Registrazione")
	public String registrazione(Utente utente,Model model) {
		Ruolo ruolo = ruoloRepository.findByIdRuolo(1);
		model.addAttribute("ruolo", ruolo);
		return "Registrazione";
	}

	@PostMapping("/Registrazione")
	public String PostRegistrazione(Model model, @RequestParam("password") String password, @RequestParam("confPassword") String confPassword,@Valid Utente utente, BindingResult bindingResult){
		
		
		if(bindingResult.hasErrors()){ //Binding associa ogni campo html col nostro oggetto html //@Valid verifica, poi associa con Binding
            return "Registrazione";
        }
        if(password.equals(confPassword)) {
        	utenteRepository.save(utente); //prende tutti i valori dell'istanza utente e li salva nel Database. Insert into Utenti..
		return "redirect:/Utente/Login"; //redirect richiesta Get (implicita)
    }
        model.addAttribute("msg", "Le password devono coincidere");
        return "Registrazione";
	}
	
	
	//CARRELLO UTENTE(ShoppingList)
	@GetMapping ("/Carrello")
	public String Carrello (Model model, HttpSession session) {
		Utente utente = (Utente) session.getAttribute("utenteAutenticato");
		if (utente != null) {
		List<ShoppingList> sl = shoppingListRepository.getListShoppingList(utente.getIdUtente());
		model.addAttribute("sl", sl);
		double totale = 0;
		for (ShoppingList slUna : sl) {
				totale = totale + slUna.getPrezzoParziale();
		}
		
		model.addAttribute("totale", totale);
		return "Carrello";
		}
		else
		return "Login";
	}
	
	
	//TASTO + ACCANTO A PRODOTTI IN CARRELLO
	@GetMapping ("/Carrello/AggiungiQuantita/{idShoppingList}")
	public String aggiungiQuantita(@PathVariable("idShoppingList") int idShoppingList) {
	ShoppingList shoppingList = shoppingListRepository.findByIdShoppingList(idShoppingList);
	int qty = shoppingList.getQuantita();
	shoppingList.setQuantita(qty + 1);
	shoppingListRepository.save(shoppingList);
	return "redirect:/Utente/Carrello";
	}
	
	
	//TASTO - ACCANTO A PRODOTTI IN CARRELLO
	@GetMapping ("/Carrello/RiduciQuantita/{idShoppingList}")
	public String riduciQuantita(@PathVariable("idShoppingList") int idShoppingList) {
	ShoppingList shoppingList = shoppingListRepository.findByIdShoppingList(idShoppingList);
	int qty = shoppingList.getQuantita();
	if (qty > 1) {
	shoppingList.setQuantita(qty - 1);
	shoppingListRepository.save(shoppingList);
	}
	return "redirect:/Utente/Carrello";
	}
	
	
	//TASTO RIMUOVI ACCANTO A PRODOTTI IN CARRELLO
	@GetMapping ("/Carrello/RimuoviShoppingList/{idShoppingList}")
	public String rimuoviShoppingList(@PathVariable("idShoppingList") int idShoppingList){
	shoppingListRepository.deleteById(idShoppingList);	
	return "redirect:/Utente/Carrello";
	}
	
	
	//AGGIUNGI PRODOTTO NEL CARRELLO(crea ShoppingList)
	@GetMapping ("/AggiungiCarrello/{idProdotto}")
	public String aggiungiCarrello(@PathVariable("idProdotto") int idProdotto, HttpSession session) {
		Utente utente = (Utente) session.getAttribute("utenteAutenticato");
		if (utente != null) {
			Prodotto prodotto = prodottoRepository.findByIdProdotto(idProdotto);
			ShoppingList shoppingList = shoppingListRepository.getSingolaShoppingList(utente.getIdUtente(), idProdotto);
			if (shoppingList != null) {
				int qty = shoppingList.getQuantita();
			    shoppingList.setQuantita(qty + 1);
			    }
			else {
			shoppingList = new ShoppingList();
			shoppingList.setUtente(utente);
			shoppingList.setProdotto(prodotto);
			shoppingList.setQuantita(1);
			}
			shoppingListRepository.save(shoppingList);
		
		return "redirect:/Utente/Carrello";
	}
		return "redirect:/Utente/Login";
	}
	
	
	//PROCEDI ALL'ORDINE (e nel nostro caso, contemporaneamente, AL PAGAMENTO)
	@GetMapping ("/Carrello/AggiungiOrdine")
	public String aggiuOrdine(HttpSession session, Model model) {
		Utente utente = (Utente) session.getAttribute("utenteAutenticato");
		StatoOrdine statoOrdine = statoOrdineRepository.findByIdStatoOrdine(3);
		if (utente != null) {
			Ordine ordine = new Ordine();
			LocalDate ora = LocalDate.now(); 
			ordine.setDataCreazione(ora);
			ordine.setStatoOrdine(statoOrdine);
			ordineRepository.save(ordine);		
			List <ShoppingList> shoppingList = shoppingListRepository.getListShoppingList(utente.getIdUtente());
			for (ShoppingList s : shoppingList) 
			{ 
				int disponibilita = shoppingListRepository.quantita(utente, s.getProdotto().getIdProdotto(), Sede.sedeOnline);
				s.setOrdine(ordine);
				if(disponibilita <0) {
					StatoOrdine statoOrdineAnnullato = statoOrdineRepository.findByIdStatoOrdine(5);
					ordine.setStatoOrdine(statoOrdineAnnullato);
					model.addAttribute("finito", "Non è presente in giacenza la quantità richiesta di " + s.getProdotto().getNome());
				}
				else {
					Giacenza giacenza = giacenzaRepository.getSingolaGiacenza(s.getProdotto().getIdProdotto(),Sede.sedeOnline);
					giacenza.setQuantita(disponibilita);
					giacenzaRepository.save(giacenza);
					model.addAttribute("ok", "Transazione avvenuta correttamente");
				}
				shoppingListRepository.save(s);
			}
			double totale = 0;
			for (ShoppingList slUna : shoppingList) {
					totale = totale + slUna.getPrezzoParziale();
			}
			Pagamento pagamento = new Pagamento();
			pagamento.setImporto(totale);
			pagamento.setDataPagamento(ora);
			pagamentoRepository.save(pagamento);
		}
		return "/Carrello";
	}
	
	
	//LISTA DEI PRODOTTI ACQUISTATI DALL'UTENTE
	@GetMapping("/Profilo/ListaProdOrdini/{id}")
	public String prodottiOrdine(@PathVariable("id") int id, HttpSession session, Model model) {
		Utente utente = (Utente) session.getAttribute("utenteAutenticato");
		Ordine ordine = ordineRepository.findByIdOrdine(id);
		
		if (ordine != null){
			List <Prodotto> listaProdOrdini = prodottoRepository.getProdottiShoppingList(utente.getIdUtente(), ordine.getIdOrdine());
        	model.addAttribute("lp", listaProdOrdini);
        	return "ListaProdOrdini";
        }
        else {
        	return "redirect:/Utente/Profilo";
        }        	
	}
		
	
	//AGGIUNGI INDIRIZZO (sul profilo personale del cliente)
	@GetMapping("/Profilo/AggiungiIndirizzo")
	public String AggiungiIndirizzo(Indirizzo indirizzo, Model model,HttpSession session) {
		Utente utente = (Utente) session.getAttribute("utenteAutenticato");
		if (utente != null) {
		model.addAttribute("utenteAut", utente);
		return "AggiungiIndirizzo"; 
		}
		return "redirect:/";
	}
	
	@PostMapping("/Profilo/AggiungiIndirizzo")
	public String postAggiungiIndirizzo(@Valid Indirizzo indirizzo, BindingResult bindingResult){
        if(bindingResult.hasErrors()){
            return "AggiungiIndirizzo";
        }
		indirizzoRepository.save(indirizzo);
		return "redirect:/Utente/Profilo";
    }
	
	
	//MODIFICA INDIRIZZO (sul profilo personale del cliente)
	@GetMapping("/Profilo/ModificaIndirizzo/{id}")
	public String modificaIndirizzo(@PathVariable("id") int id, Model model) {
		Indirizzo indirizzo = indirizzoRepository.findById(id);
		if (indirizzo != null){
        	model.addAttribute("indirizzo", indirizzo);
        	return "ModificaIndirizzo";
        }
        else {
        	return "redirect:/Utente/Profilo";
        }        	
	}
	
	@PostMapping("/Profilo/ModificaIndirizzo")
	public String postModificaIndirizzo(@Valid Indirizzo indirizzo, BindingResult bindingResult, Model model) {
        if(bindingResult.hasErrors()){
            return "ModificaIndirizzo";
        }
        indirizzoRepository.save(indirizzo);
		
        return "redirect:/Utente/Profilo";
	}
	
	
	//RIMUOVI INDIRIZZO (sul profilo personale del cliente)
	@GetMapping("/Profilo/RimuoviIndirizzo/{id}")
	public String rimuoviIndirizzo(@PathVariable("id") int id) {
		indirizzoRepository.deleteById(id);
		return "redirect:/Utente/Profilo";
	}
	
}






