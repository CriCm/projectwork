package it.KitchenInLove.ProjectWork.Controller;


import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import it.KitchenInLove.ProjectWork.Dao.PagamentoDao;
import it.KitchenInLove.ProjectWork.Dao.ShoppingListDao;
import it.KitchenInLove.ProjectWork.Model.Prodotto;
import it.KitchenInLove.ProjectWork.Model.ShoppingList;

@RestController
public class ReportRestController {
	@Autowired
	ShoppingListDao shoppingListRepository;
	@Autowired
	PagamentoDao pagamentoRepository;
	
	//Totale vendite per prodotto
	//{"nomeProdotto":["Mestolo","Set cucchiaini","Contenitore Vetro","Caffettiera"],"quantitaProdotto":[16,8,9,53]}
	@GetMapping ("/Admin/ReportSLLite")
	public Map<String, ArrayList<Object>> getShoppingListLite() {
		List<ShoppingList> lista= shoppingListRepository.nomiProdotto();
		List<Integer> listaQty= shoppingListRepository.quantitaProdotto();
		Map<String, ArrayList<Object>> mapLista = new HashMap<String, ArrayList<Object>>();
		ArrayList<Object> nomi = new ArrayList<Object>();
		ArrayList<Object> qty = new ArrayList<Object>();
		for (ShoppingList s: lista) {
			nomi.add(s.getProdotto().getNome());
		}
		for (Integer i: listaQty) {
			qty.add(i);
		}
		mapLista.put("nomeProdotto", nomi);
		mapLista.put("quantitaProdotto", qty);
		    return mapLista;	
		
	}

	
	//Totale fatturato anno corrente
	@GetMapping ("/Admin/ReportFatturato")
	public Map<String, ArrayList<Object>> getFatturato() {
		
		Map<String, ArrayList<Object>> mapLista = new HashMap<String, ArrayList<Object>>();
		
		ArrayList<Object> fatturato = new ArrayList<Object>();
		LocalDate data = LocalDate.now();
		int anno = data.getYear();;
		for (int i=1; i<=12; i++) {
			Double mese=pagamentoRepository.fatturatoSemplice(i, anno);
			if(mese != null)
			fatturato.add(mese);
			else
				fatturato.add(0.0);
		}
		ArrayList<Object> mesi = new ArrayList<Object>();
		mesi.add("Gennaio");
		mesi.add("Febbraio");
		mesi.add("Marzo");
		mesi.add("Aprile");
		mesi.add("Maggio");
		mesi.add("Giugno");
		mesi.add("Luglio");
		mesi.add("Agosto");
		mesi.add("Settembre");
		mesi.add("Ottobre");
		mesi.add("Novembre");
		mesi.add("Dicembre");
		
		mapLista.put("nomeMese", mesi);
		mapLista.put("fatturato", fatturato);
		    return mapLista;	
	}
	
	
	//Confronto tra 2 fatturati in un mese e anno incati dall'utente
	@GetMapping("/Admin/ReportPerData")
	public Map<String, ArrayList<Object>> getFatturatoPerData(@RequestParam("mese") int mese, @RequestParam("anno") int anno, @RequestParam("mese2") int mese2, @RequestParam("anno2") int anno2) { 
		Double importo = pagamentoRepository.fatturatoSemplice(mese, anno);
		Double importo2 = pagamentoRepository.fatturatoSemplice(mese2, anno2);
		
		Map<String, ArrayList<Object>> mapLista = new HashMap<String, ArrayList<Object>>();
		ArrayList<Object> fatturato = new ArrayList<Object>();
		fatturato.add(importo);
		fatturato.add(importo2);
		mapLista.put("fatturato", fatturato);
		    return mapLista;	
	}
	
	
	//I 5 prodotti più venduti
	@GetMapping ("/Admin/reportPiuVenduti/")
	public Map<String, ArrayList<Object>> reportPiuVenduti (){
		Pageable pageable = PageRequest.of(0, 5);
		List <Prodotto> piuVenduti = shoppingListRepository.piuVenduti(pageable);
		List<Integer> listaQty= shoppingListRepository.quantitaProdottoDesc(pageable);
		Map<String, ArrayList<Object>> mapLista = new HashMap<String, ArrayList<Object>>();
		ArrayList<Object> nomi = new ArrayList<Object>();
		ArrayList<Object> qty = new ArrayList<Object>();
		for (Prodotto p: piuVenduti) {
			nomi.add(p.getNome());
		}
		for (Integer i: listaQty) {
			qty.add(i);
		}
		mapLista.put("nomeProdotto", nomi);
		mapLista.put("quantitaProdotto", qty);
		    return mapLista;			
	}
	
	
	//I 5 prodotti meno venduti
	@GetMapping ("/Admin/reportMenoVenduti/")
	public Map<String, ArrayList<Object>> reportMenoVenduti (){
		Pageable pageable = PageRequest.of(0, 5);
		List <Prodotto> menoVenduti = shoppingListRepository.menoVenduti(pageable);
		List<Integer> listaQty= shoppingListRepository.quantitaProdottoAsc(pageable);
		Map<String, ArrayList<Object>> mapLista = new HashMap<String, ArrayList<Object>>();
		ArrayList<Object> nomi = new ArrayList<Object>();
		ArrayList<Object> qty = new ArrayList<Object>();
		for (Prodotto p: menoVenduti) {
			nomi.add(p.getNome());
		}
		for (Integer i: listaQty) {
			qty.add(i);
		}
		mapLista.put("nomeProdotto", nomi);
		mapLista.put("quantitaProdotto", qty);
		    return mapLista;			
	}
}
