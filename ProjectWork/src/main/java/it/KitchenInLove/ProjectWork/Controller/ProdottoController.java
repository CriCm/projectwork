package it.KitchenInLove.ProjectWork.Controller;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import it.KitchenInLove.ProjectWork.Dao.CategoriaDao;
import it.KitchenInLove.ProjectWork.Dao.GiacenzaDao;
import it.KitchenInLove.ProjectWork.Dao.ImmagineDao;
import it.KitchenInLove.ProjectWork.Dao.ListaPromoDao;
import it.KitchenInLove.ProjectWork.Dao.MarcaDao;
import it.KitchenInLove.ProjectWork.Dao.ProdottoDao;
import it.KitchenInLove.ProjectWork.Dao.PromozioneDao;
import it.KitchenInLove.ProjectWork.Dao.ShoppingListDao;
import it.KitchenInLove.ProjectWork.Model.Immagine;
import it.KitchenInLove.ProjectWork.Model.Prodotto;
import it.KitchenInLove.ProjectWork.Model.Promozione;
import it.KitchenInLove.ProjectWork.Model.Sede;

@Controller
@RequestMapping(path="/Prodotto")
public class ProdottoController {
	
	@Autowired 
	ProdottoDao prodottoRepository;
	@Autowired 
	CategoriaDao categoriaRepository;
	@Autowired
	ImmagineDao immagineRepository;
	@Autowired
	PromozioneDao promozioneRepository;
	@Autowired
	ListaPromoDao listaPromoRepository;
	@Autowired
	ShoppingListDao shoppingListRepository;
	@Autowired
	MarcaDao marcaRepository;
	@Autowired
	GiacenzaDao giacenzaRepository;
	
	//LISTA PRODOTTI 
	//filtrata in base alla promozione selezionata, la categoria o la marca
	@GetMapping("/ListaProdotti")
	public String ListaProdotti(Model model, @RequestParam("idPromo") Optional<Integer> idPromo, @RequestParam("idCategoria") Optional<Integer> idCategoria, @RequestParam("idMarca") Optional<Integer> idMarca) {
		List <Prodotto> listaProdotti = prodottoRepository.findAll();
		if (idPromo.isPresent()) {    
			Promozione promozione = promozioneRepository.findByIdPromozione(idPromo);
			listaProdotti = listaPromoRepository.cercaPerIdPromo(idPromo);
			controlloGiacenza(listaProdotti);
			model.addAttribute("Promozione", promozione);
		}
		else if (idCategoria.isPresent()) {
			listaProdotti = prodottoRepository.findByCategoriaIdCategoria(idCategoria);
			controlloGiacenza(listaProdotti);
		}
		else if (idMarca.isPresent()) {
			listaProdotti = marcaRepository.prodMarca(idMarca);
			controlloGiacenza(listaProdotti);
		}
		model.addAttribute("PHProdotti", listaProdotti);
		return "ListaProdotti"; 
	}
	
	
	//CONTROLLA SE I PRODOTTI SONO PRESENTI NEL MAGAZZINO ADIBITO ALLA VENDITA ONLINE(e sono quindi acquistabili)
	public void controlloGiacenza (List<Prodotto> listaProdotti) {
		List <Prodotto> prodInGia = prodottoRepository.prodottiInGiacenza(Sede.sedeOnline);
		for (int i = 0; i<listaProdotti.size(); i++) {
			int trovato = 0;
			int idProdotto = listaProdotti.get(i).getIdProdotto();
			  for (int y = 0; y<prodInGia.size(); y++) {
				  int idProdGiac = prodInGia.get(y).getIdProdotto();
				  if (idProdotto == idProdGiac) {
					  if(giacenzaRepository.getQuantita(idProdotto,Sede.sedeOnline) > 0)  
						  trovato++;
				  }
			  }
			  if (trovato == 0) {
				     Prodotto prodotto = prodottoRepository.findByIdProdotto(idProdotto);
				     prodotto.setEsaurito(true);
			  }
		}	
    }
	
	
	//CONTROLLA SE IL PRODOTTO E' PRESENTI NEL MAGAZZINO ADIBITO ALLA VENDITA ONLINE(ed è quindi acquistabile)
	public int controlloGiacenzaSingolo (Prodotto prodotto) {
		List <Prodotto> prodInGia = prodottoRepository.prodottiInGiacenza(Sede.sedeOnline);
		int trovato = 0;
		int qty = 0;
		for (int i = 0; i<prodInGia.size(); i++) {
			int idProdotto = prodInGia.get(i).getIdProdotto();
			if (idProdotto == prodotto.getIdProdotto()) {
				qty = giacenzaRepository.getQuantita(idProdotto, Sede.sedeOnline);
				if(qty > 0)
					trovato++;
			}
		}
		if (trovato == 0)
		     prodotto.setEsaurito(true);
		return qty;
	}	
  
	
	//LISTA DELLE CATEGORIE
	@GetMapping("/Categorie")
	public String Categorie(Model model) {
		model.addAttribute("catPrincipali", categoriaRepository.categoriePrincipali());
		return "Categorie"; 
	}
	
	
	//DETTAGLIO DI UN DETERMINATO PRODOTTO (tutti gli attributi + eventuale promozione in cui si trova e relativo prezzo scontato)
	@GetMapping("/Dettaglio/{id}")
	public String Dettaglio (@PathVariable("id") int id, Model model) {              
		Prodotto prodotto = prodottoRepository.findByIdProdotto(id);
          if (prodotto != null) {
        	   int qty = controlloGiacenzaSingolo(prodotto);
      		   if(qty >0 && qty <5)
      			   model.addAttribute("pochi", "Ne rimangono solo " + qty);
      			   
        	   List <Immagine> immagini = immagineRepository.cercaPerIdProdottoImmPrincipale(prodotto.getIdProdotto());
        	   List <Immagine> immaginiSecondarie = immagineRepository.cercaPerIdProdottoImmSecondarie(prodotto.getIdProdotto());
               model.addAttribute("PHProdotto", prodotto);
               model.addAttribute("PHImmagine", immagini);
               model.addAttribute("PHImmagineSec", immaginiSecondarie);
               List <Promozione> promCorr = promozioneRepository.promozioneAttuale();
       		   if (promCorr != null) {
       			for (Promozione promo : promCorr) { 
       			    model.addAttribute("promo", promo);
       			}
       		   }
            return "DettaglioProdotto";
        } 
        else {
            return null;
        }        
	}	
	
	
	//LISTA DEI 10 PRODOTTI PIù ACQUISTATI
	@GetMapping ("/PiuVenduti")
	public String piuVenduti (Model model) {
		Pageable pageable = PageRequest.of(0, 10);
		List <Prodotto> piuVenduti = shoppingListRepository.piuVenduti(pageable);
		model.addAttribute("PHProdotti", piuVenduti);
		return "ListaProdotti";
	}

	
	//LISTA DELLE PROMOZIONI ATTIVE AL MOMENTO E NEI PROSSIMI 30 GIORNI
	@GetMapping("/Promozioni")
	public String promozioniCorrenti (Model model) { 
		List <Promozione> promCorr = promozioneRepository.promozioniCorrenti();
		if (promCorr.size() == 0) {
			model.addAttribute("errpromo", "Non sono presenti promozioni correnti.");
			return "Promozioni";
		}
		else
			model.addAttribute("PHPromozioni", promCorr);
		return "Promozioni"; 
	}
 }