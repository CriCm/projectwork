package it.KitchenInLove.ProjectWork.Controller;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import it.KitchenInLove.ProjectWork.Dao.CategoriaDao;
import it.KitchenInLove.ProjectWork.Dao.GiacenzaDao;
import it.KitchenInLove.ProjectWork.Dao.ListaPromoDao;
import it.KitchenInLove.ProjectWork.Dao.MarcaDao;
import it.KitchenInLove.ProjectWork.Dao.ProdottoDao;
import it.KitchenInLove.ProjectWork.Dao.SedeDao;
import it.KitchenInLove.ProjectWork.Model.Prodotto;
import it.KitchenInLove.ProjectWork.Model.Sede;

@Controller
public class HomeController {
	
	@Autowired 
	ProdottoDao prodottoRepository;
	@Autowired
	ListaPromoDao listaPromoRepository;
	@Autowired
	SedeDao sedeRepository;
	@Autowired 
	MarcaDao marcaRepository;
	@Autowired
	CategoriaDao categoriaRepository;
	@Autowired
	GiacenzaDao giacenzaRepository;
	
	
	//HOMEPAGE DEL SITO (link a tutte le diverse pagine e lista di max 8 prodotti che sono stati venduti meno)
	@GetMapping("/")
	public String Homepage(Model model) { 
		List <Prodotto> listaProdotti = prodottoRepository.menoVendutiDaMostrareSuHome();
		controlloGiacenza(listaProdotti);
		if (listaProdotti.size()>=8)
		listaProdotti = listaProdotti.subList(0, 8);
		model.addAttribute("PHProdotti", listaProdotti);
		model.addAttribute("catPrincipali", categoriaRepository.categoriePrincipali());
		
		return "Homepage"; 
	}
	
	
	//CONTROLLA SE I PRODOTTI SONO PRESENTI NEL MAGAZZINO ADIBITO ALLA VENDITA ONLINE(e sono quindi acquistabili)
	public void controlloGiacenza (List<Prodotto> listaProdotti) {
		List <Prodotto> prodInGia = prodottoRepository.prodottiInGiacenza(Sede.sedeOnline);
		for (int i = 0; i<listaProdotti.size(); i++) {
			int trovato = 0;
			int idProdotto = listaProdotti.get(i).getIdProdotto();
			  for (int y = 0; y<prodInGia.size(); y++) {
				  int idProdGiac = prodInGia.get(y).getIdProdotto();
				  if (idProdotto == idProdGiac) {
					  if(giacenzaRepository.getQuantita(idProdotto,Sede.sedeOnline) > 0)
						  trovato++;
				      }
			         }
			  if (trovato == 0) {
				     Prodotto prodotto = prodottoRepository.findByIdProdotto(idProdotto);
				     prodotto.setEsaurito(true);
			  }
		}	
    }
	
	
	//CHI SIAMO
	@GetMapping("/ChiSiamo")
	public String ChiSiamo() { 
		return "ChiSiamo"; 
	}
	
	
	//CONTATTI (mappa di Google Maps + recapiti sede Centrale con mail già preimpostata)
	@GetMapping("/Contatti")
	public String Contatti () { 
		return "Contatti"; 
	}
	
	
	//FAQ
	@GetMapping("/FAQ")
	public String FAQ() { 
		return "FAQ"; 
	}
	
	
	//PRIVACY
	@GetMapping("/Privacy")
	public String Privacy () { 
		return "Privacy"; 
	}
	
	
	//LISTA DI TUTTI I NEGOZI
	@GetMapping("/Sedi")
	public String Sedi(Model model){
		model.addAttribute("sede", sedeRepository.findAll());
		return "Sedi";
    }	
	
	
	//LISTA DI TUTTE LE MARCHE CHE SONO STATE IDENTIFICATE COME PRINCIPALI TRAMITE ATTRIBUTO GRANDE
	@GetMapping("/GrandiMarche")
	public String GrandiMarche (Model model) {
		model.addAttribute("grandimarche", marcaRepository.grandiMarche());
		return "GrandiMarche";
	}
	
	
	//STRUMENTO PER LA RICERCA DEI PRODOTTI ATTRAVERSO PARTE DEL NOME O DELLA BREVE DESCRIZIONE
	@PostMapping("/")
	public String postCerca(@RequestParam("stringa") String stringa, Model model) { 
		
		if (prodottoRepository.cerca(stringa) == null) {
			return "redirect:/";
		}
		else {
			List <Prodotto> prova = prodottoRepository.cerca(stringa);
			model.addAttribute("PHProdotti", prova);
			
		return "ListaProdotti"; 
		}
	}
}


