package it.KitchenInLove.ProjectWork.Controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import it.KitchenInLove.ProjectWork.Dao.GiacenzaDao;
import it.KitchenInLove.ProjectWork.Dao.ProdottoDao;
import it.KitchenInLove.ProjectWork.Dao.SedeDao;
import it.KitchenInLove.ProjectWork.Dao.ShoppingListDao;
import it.KitchenInLove.ProjectWork.Model.Giacenza;
import it.KitchenInLove.ProjectWork.Model.Prodotto;
import it.KitchenInLove.ProjectWork.Model.Sede;

@RestController
public class GiacenzaRestController {

	@Autowired
	SedeDao sedeRepository;
	@Autowired
	ProdottoDao prodottoRepository;
	@Autowired
	GiacenzaDao giacenzaRepository;
	@Autowired
	ShoppingListDao shoppingListRepository;
		
	
		//LISTA DEI PRODOTTI ASSOCIATI AD UNA SEDE CON LA RELATIVA QUANTITÀ
		//[{"id":1,"idSede":1,"quantita":9},{"id":2,"idSede":1,"quantita":3},{"id":3,"idSede":1,"quantita":5},{"id":4,"idSede":1,"quantita":0},{"id":5,"idSede":1,"quantita":5}]
		@GetMapping("/Admin/Giacenza/{id}")
		public ArrayList<Map<String, Object>> giacenza(@PathVariable("id") int id){
			System.out.println(id);
			List <Giacenza> giacenzaInSede = giacenzaRepository.giacenzaInSede(id);
			
			int lung = giacenzaInSede.size();
			ArrayList<Map<String, Object>> json = new ArrayList<Map<String, Object>>();
				for(int i = 0; i < lung; i++) {
					int idProd = giacenzaInSede.get(i).getProdotto().getIdProdotto();
					int idSede = giacenzaInSede.get(i).getSede().getIdSede();
					Integer qt = giacenzaInSede.get(i).getQuantita();
					Map<String, Object> diz = new HashMap<String, Object>();
					diz.put("id", idProd);
					diz.put("idSede", idSede);
					diz.put("quantita", qt);
					json.add(diz);
				}
				
			return json;
		}
	
	
		//SE UN PRODOTTO È GIÀ PRESENTE IN UNA SEDE NE AGGIORNA LA QUANTITÀ, ALTRIMENTI CREA UNA NUOVA GIACENZA
		@PostMapping("/Admin/Giacenza/{id}")
		public void postGiacenza(@RequestBody String stringa, @PathVariable("id") int id){			
			JSONArray array = new JSONArray(stringa);  
			for(int i=0; i < array.length(); i++)   {  
				JSONObject object = array.getJSONObject(i);  
				int idProdotto = object.getInt("id");
				int idSede = object.getInt("idSede");
				int qty = object.getInt("quantita");
				Prodotto prodotto = prodottoRepository.findByIdProdotto(idProdotto);
				Sede sede = sedeRepository.findByIdSede(idSede);
				
				Giacenza g = giacenzaRepository.getSingolaGiacenza(prodotto.getIdProdotto(), sede.getIdSede());
					if (g == null) {
						g = new Giacenza();
						g.setProdotto(prodotto);
						g.setSede(sede);
						
					}
				g.setQuantita(qty);
				giacenzaRepository.save(g);
			}
		}
}
