package it.KitchenInLove.ProjectWork.Controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import it.KitchenInLove.ProjectWork.Dao.GiacenzaDao;
import it.KitchenInLove.ProjectWork.Dao.ProdottoDao;
import it.KitchenInLove.ProjectWork.Dao.SedeDao;
import it.KitchenInLove.ProjectWork.Model.Prodotto;
import it.KitchenInLove.ProjectWork.Model.Sede;
import it.KitchenInLove.ProjectWork.Model.Utente;

@Controller
@RequestMapping(path="/Giacenza")
public class GiacenzaController {
	@Autowired
	SedeDao sedeRepository;
	@Autowired 
	ProdottoDao prodottoRepository;
	@Autowired
	GiacenzaDao giacenzaRepository;
	
	
	//SE L'UTENTE CHE EFFETTUA IL LOGIN HA RUOLO CON ID=3, RESTITUISCE UNA PAGINA IN CUI è POSSIBILE GESTIRE LA GIACENZA DI TUTTE LE SEDI
	@GetMapping("/")
	public String Giacenza(Model model, HttpSession session) { 
		List <Prodotto> listaProdotti = prodottoRepository.findAll();
		List <Sede> listaSedi = sedeRepository.findAll();
		model.addAttribute("prodotti", listaProdotti);
		model.addAttribute("sedi", listaSedi);
		Utente utente = (Utente) session.getAttribute("utenteAutenticato");
		if (utente != null && utente.getRuolo().getIdRuolo() == 3) {
			return "Giacenza";
		}
		return "redirect:/Utente/Login";
	}
}
