package it.KitchenInLove.ProjectWork.Controller;

import java.util.List;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import it.KitchenInLove.ProjectWork.Dao.CategoriaDao;
import it.KitchenInLove.ProjectWork.Dao.GiacenzaDao;
import it.KitchenInLove.ProjectWork.Dao.ImmagineDao;
import it.KitchenInLove.ProjectWork.Dao.ListaPromoDao;
import it.KitchenInLove.ProjectWork.Dao.MarcaDao;
import it.KitchenInLove.ProjectWork.Dao.OrdineDao;
import it.KitchenInLove.ProjectWork.Dao.ProdottoDao;
import it.KitchenInLove.ProjectWork.Dao.PromozioneDao;
import it.KitchenInLove.ProjectWork.Dao.RuoloDao;
import it.KitchenInLove.ProjectWork.Dao.SedeDao;
import it.KitchenInLove.ProjectWork.Dao.ShoppingListDao;
import it.KitchenInLove.ProjectWork.Dao.UtenteDao;
import it.KitchenInLove.ProjectWork.Model.Categoria;
import it.KitchenInLove.ProjectWork.Model.Immagine;
import it.KitchenInLove.ProjectWork.Model.ListaPromo;
import it.KitchenInLove.ProjectWork.Model.Marca;
import it.KitchenInLove.ProjectWork.Model.Prodotto;
import it.KitchenInLove.ProjectWork.Model.Promozione;
import it.KitchenInLove.ProjectWork.Model.Ruolo;
import it.KitchenInLove.ProjectWork.Model.Sede;
import it.KitchenInLove.ProjectWork.Model.ShoppingList;
import it.KitchenInLove.ProjectWork.Model.Utente;

@Controller
@RequestMapping(path="/Admin")
public class AdminController {
	
	@Autowired
	ProdottoDao prodottoRepository;
	@Autowired
	CategoriaDao categoriaRepository;
	@Autowired
	MarcaDao marcaRepository;
	@Autowired
	SedeDao sedeRepository;
	@Autowired
	OrdineDao ordineRepository;
	@Autowired
	RuoloDao ruoloRepository;
	@Autowired
	PromozioneDao promozioneRepository;
	@Autowired
	ShoppingListDao shoppingListRepository;
	@Autowired
	UtenteDao utenteRepository;
	@Autowired
	ImmagineDao immagineRepository;
	@Autowired
	ListaPromoDao listaPromoRepository;
	@Autowired
	GiacenzaDao giacenzaRepository;
	

	//HOMEPAGE LATO AMMINISTRATORE ACCESSIBILE SOLO SE CHI EFFETTUA IL LOGIN HA RUOLO=2 : GRAFICI RELATIVI ALLE VENDITE E MENU LATERALE
	@GetMapping("/")
	public String latoAdmin(Model model, HttpSession session) { 
		Utente utente = (Utente) session.getAttribute("utenteAutenticato");
		if (utente != null && utente.getRuolo().getIdRuolo() == 2) {
			return "LatoAdmin";
		}
		return "redirect:/Utente/Login"; 
	}
	
	
	//LISTA DI TUTTI I PRODOTTI CON LINK PER LA MODIFICA, LA RIMOZIONE E L'AGGIUNTA
	@GetMapping("/EditorProdotti")
	public String editorProdotti(Model model, HttpSession session){
		Utente utente = (Utente) session.getAttribute("utenteAutenticato");
		if (utente != null && utente.getRuolo().getIdRuolo() == 2 ) {
		model.addAttribute("prodotto", prodottoRepository.findAll());
		return "EditorProdotti";
		}
		return "redirect:/Utente/Login";
	}
	
	
	//AGGIUNGE UN PRODOTTO NEL DATABASE
	@GetMapping("/EditorProdotti/AggiungiProdotti")
	public String aggiungiProdotti(Prodotto prodotto, Model model) {
		model.addAttribute("marca", marcaRepository.findAll());
        model.addAttribute("categoria", categoriaRepository.findAll());
		return "AggiungiProdotti"; 
	}
	
	@PostMapping("/EditorProdotti/AggiungiProdotti")
	public String postAggiungiProdotti(@Valid Prodotto prodotto, BindingResult bindingResult, Model model){
        if(bindingResult.hasErrors()){
            return "AggiungiProdotti";
        }
        model.addAttribute("marca", marcaRepository.findAll());
        model.addAttribute("categoria", categoriaRepository.findAll());
		prodottoRepository.save(prodotto);
		return "redirect:/Admin/EditorProdotti";
    }
	
	
	//AGGIUNGE UN IMMAGINE COLLEGATA AL PRODOTTO SELEZIONATO 
	//(Andrebbero implementate la modifica, la rimozione e i controlli relativi all'immagine Principale, ma si è deciso di non farlo)
	@GetMapping("/EditorProdotti/AggiungiImmagine/{id}")
	public String aggiungiImmagine(Immagine immagine, @PathVariable("id") int id, Model model) {
		Prodotto prodotto = prodottoRepository.findByIdProdotto(id);
        model.addAttribute("prodotto", prodotto);
		return "AggiungiImmagine"; 
	}
	
	@PostMapping("/EditorProdotti/AggiungiImmagine")
	public String postAggiungiImmagine(@Valid Immagine immagine,  BindingResult bindingResult){
        if(bindingResult.hasErrors()){
            return "AggiungiImmagine";
        }
		immagineRepository.save(immagine);
		return "redirect:/Admin/EditorProdotti";
    }
	
	
	//MODIFICA IL PRODOTTO SELEZIONATO
	@GetMapping("/EditorProdotti/Modifica/{id}")
	public String modificaProdotti(@PathVariable("id") int id, Model model) {
		Prodotto prodotto = prodottoRepository.findByIdProdotto(id);

        	model.addAttribute("prodotto", prodotto);
        	model.addAttribute("marca", marcaRepository.findAll());
        	model.addAttribute("categoria", categoriaRepository.findAll());
        	
            return "ModificaProdotti";
	}
	
	@PostMapping("/EditorProdotti/Modifica")
	public String postModificaProdotti(@Valid Prodotto prodotto, BindingResult bindingResult, Model model) {
        if(bindingResult.hasErrors()){
        	model.addAttribute("marca", marcaRepository.findAll());
        	model.addAttribute("categoria", categoriaRepository.findAll());
            return "ModificaProdotti";
        }
        prodottoRepository.save(prodotto);
		
        return "redirect:/Admin/EditorProdotti";
	}
	
	
	//RIMUOVE IL PRODOTTO DAL DATABASE
	@GetMapping("/EditorProdotti/Rimuovi/{id}")
	public String rimuoviProdotto(@PathVariable("id") int id) {
		prodottoRepository.deleteById(id);
		return "redirect:/Admin/EditorProdotti";
	}
	
	
	//LISTA DI TUTTE LE SEDI/NEGOZI
	@GetMapping("/EditorSedi")
	public String editorSedi(Model model, HttpSession session){
		Utente utente = (Utente) session.getAttribute("utenteAutenticato");
		if (utente != null && utente.getRuolo().getIdRuolo() == 2) {
		model.addAttribute("sede", sedeRepository.findAll());
		return "EditorSedi";
		}
		return "redirect:/Utente/Login";
	}
	
	
	//AGGIUNGE UNA SEDE/NEGOZIO
	@GetMapping("/EditorSedi/AggiungiSede")
	public String aggiungiSede(Sede sede) {
		return "AggiungiSede"; 
	}
	
	@PostMapping("/EditorSedi/AggiungiSede")
	public String postAggiungiSede(@Valid Sede sede, BindingResult bindingResult){
        if(bindingResult.hasErrors()){
            return "AggiungiSede";
        }
		sedeRepository.save(sede);
		return "redirect:/Admin/EditorSedi";
    }
	
	
	//MODIFICA LA SEDE SELEZIONATA
	@RequestMapping("/EditorSedi/ModificaSede/{id}")
	public String modificaSede(@PathVariable("id") int id, Model model) {
		Sede sede = sedeRepository.findByIdSede(id);

        if (sede != null){
        	model.addAttribute("sede", sede);
        	return "ModificaSede";
        }
        else {
        	return "redirect:/Admin/EditorSedi";
        }
	}
	
	@PostMapping("/EditorSedi/ModificaSede")
	public String postModificaSede(@Valid Sede sede, BindingResult bindingResult, Model model) {
        if(bindingResult.hasErrors()){
            return "ModificaSede";
        }
        sedeRepository.save(sede);
        return "redirect:/Admin/EditorSedi";
	}
	
	
	//RIMUOVE LA SEDE SELEZIONATA
	@GetMapping("/EditorSedi/RimuoviSede/{id}")
	public String rimuoviSede(@PathVariable("id") int id) {
		sedeRepository.deleteById(id);
		return "redirect:/Admin/EditorSedi";
	}
	
	
	//RESTITUISCE LA LISTA DEGLI UTENTI CON IL RELATIVO RUOLO/MANSIONE
	@GetMapping("/ListaUtenti")
	public String ListaUtenti(Model model, HttpSession session){
		Utente utente = (Utente) session.getAttribute("utenteAutenticato");
		if (utente != null && utente.getRuolo().getIdRuolo() == 2) {
		model.addAttribute("utente", utenteRepository.findAll());
		return "ListaUtenti";
		}
		return "redirect:/Utente/Login";
	}
	
	
	//MODIFICA ATTRAVERSO UN MENù A TENDINA L'ATTUALE RUOLO/MANSIONE DELL'UTENTE SELEZIONATO
	@RequestMapping("/ListaUtenti/AssegnaMansioni/{id}")
	public String AssegnaMansioni(@PathVariable("id") int id, Model model) {
		Utente utente = utenteRepository.findByIdUtente(id);
        if (utente != null){
        	model.addAttribute("utente", utente);
        	model.addAttribute("ruolo", ruoloRepository.findAll());
        	return "AssegnaMansioni";
        }
        else {
        	return "redirect:/Admin/ListaUtenti";
        }
	}
	
	@PostMapping("/ListaUtenti/AssegnaMansioni")
	public String postAssegnaMansioni(@Valid Utente utente, BindingResult bindingResult, Model model) {
        if(bindingResult.hasErrors()){
        	model.addAttribute("ruolo", ruoloRepository.findAll());
            return "AssegnaMansioni";
        }
        utenteRepository.save(utente);
        return "redirect:/Admin/ListaUtenti";
	}
	
	
	
	//LISTA DI TUTTE LE CATEGORIE CON POSSIBILITà DI AGGIUNTA/MODIFICA/RIMOZIONE
	@GetMapping("/EditorCategorie")
	public String editorCategorie(Model model,HttpSession session){
		Utente utente = (Utente) session.getAttribute("utenteAutenticato");
		if (utente != null && utente.getRuolo().getIdRuolo() == 2) {
		model.addAttribute("categoria", categoriaRepository.findAll());
		return "EditorCategorie";
		}
		return "redirect:/Utente/Login";
	}
	
	
	//AGGIUNGE UNA CATEGORIA NEL DATABASE
	@GetMapping("/EditorCategorie/AggiungiCategoria")
	public String aggiungiCategoria(Categoria categoria) {
		return "AggiungiCategoria"; 
	}
	
	@PostMapping("/EditorCategorie/AggiungiCategoria")
	public String postAggiungiCategoria(@Valid Categoria categoria, BindingResult bindingResult){
        if(bindingResult.hasErrors()){
            return "AggiungiCategoria";
        }
		categoriaRepository.save(categoria);
		return "redirect:/Admin/EditorCategorie";
    }
	
	
	//MODIFICA LA CATEGORIA SELEZIONATA
	@RequestMapping("/EditorCategorie/ModificaCategoria/{id}")
	public String modificaCategorie(@PathVariable("id") int id, Model model) {
		Categoria categoria = categoriaRepository.findByIdCategoria(id);

        if (categoria != null){
        	model.addAttribute("categoria", categoria);
        	model.addAttribute("idGenitore", categoriaRepository.findAll());
        	return "ModificaCategoria";
        }
        else {
            return "redirect:/Admin/EditorCategorie";
        }
	}
	
	@PostMapping("/EditorCategorie/ModificaCategoria")
	public String postModificaCategoria(@Valid Categoria categoria, BindingResult bindingResult, Model model) {
        if(bindingResult.hasErrors()){
        	model.addAttribute("idGenitore", categoriaRepository.findAll());
            return "ModificaCategoria";
        }
        categoriaRepository.save(categoria);
        return "redirect:/Admin/EditorCategorie";
	}
	
	
	//ELIMINA LA CATEGORIA SELEZIONATA
	@GetMapping("/EditorCategorie/RimuoviCategoria/{id}")
	public String rimuoviCategoria(@PathVariable("id") int id) {
		categoriaRepository.deleteById(id);
		return "redirect:/Admin/EditorCategorie";
	}
	
	
	//LISTA DI TUTTI GLI ORDINI EFFETTUATI DAGLI UTENTI IN ORDINE DAL PIù RECENTE AL PIù VECCHIO
	@GetMapping("/EditorOrdini")
	public String ordini(Model model, HttpSession session) { 
		Utente utente = (Utente) session.getAttribute("utenteAutenticato");
		if (utente != null && utente.getRuolo().getIdRuolo() == 2) {
		    List <ShoppingList> sl = shoppingListRepository.getOrdiniEffettuati();
		    model.addAttribute("sl", sl);
		return "EditorOrdini"; 
		}
		return "redirect:/Utente/Login";
	}

	
	//LISTA DEI RUOLI CON RELATIVA DESCRIZIONE E POSSIBILITà DI MODIFICA
	@GetMapping("/EditorRuoli")
	public String editorRuoli(Model model, HttpSession session){
		Utente utente = (Utente) session.getAttribute("utenteAutenticato");
		if (utente != null && utente.getRuolo().getIdRuolo() == 2) {
		model.addAttribute("ruolo", ruoloRepository.findAll());
		return "EditorRuoli";
		}
		return "redirect:/Utente/Login";
	}
	
	
	//AGGIUNGE UN RUOLO
	@GetMapping("/EditorRuoli/AggiungiRuolo")
	public String aggiungiRuolo(Ruolo ruolo) {
		return "AggiungiRuolo"; 
	}
	
	@PostMapping("/EditorRuoli/AggiungiRuolo")
	public String postAggiungiRuolo(@Valid Ruolo ruolo, BindingResult bindingResult){
        if(bindingResult.hasErrors()){
            return "AggiungiRuolo";
        }
		ruoloRepository.save(ruolo);
		return "redirect:/Admin/EditorRuoli";
    }
	
	
	//MODIFICA IL RUOLO SELEZIONATO
	@RequestMapping("/EditorRuoli/ModificaRuolo/{id}")
	public String modificaRuolo(@PathVariable("id") int id, Model model) {
		Ruolo ruolo = ruoloRepository.findByIdRuolo(id);

        if (ruolo != null){
        	model.addAttribute("ruolo", ruolo);
        	return "ModificaRuolo";
        }
        else {
            return "redirect:/Admin/EditorRuoli";
        }
	}
	
	@PostMapping("/EditorRuoli/ModificaRuolo")
	public String postModificaRuolo(@Valid Ruolo ruolo, BindingResult bindingResult, Model model) {
        if(bindingResult.hasErrors()){
            return "ModificaRuolo";
        }
        ruoloRepository.save(ruolo);
        return "redirect:/Admin/EditorRuoli";
	}
	
	
	//RIMUOVE IL RUOLO SELEZIONATO
	@GetMapping("/EditorRuoli/RimuoviRuolo/{id}")
	public String rimuoviRuolo(@PathVariable("id") int id) {
		ruoloRepository.deleteById(id);
		return "redirect:/Admin/EditorRuoli";
	}
	
	
	//LISTA DI TUTTE LE PROMOZIONI
	@GetMapping("/EditorPromozioni")
	public String editorPromozioni(Model model, HttpSession session){
		Utente utente = (Utente) session.getAttribute("utenteAutenticato");
		if (utente != null && utente.getRuolo().getIdRuolo() == 2) {
		model.addAttribute("promozione", promozioneRepository.findAll());
		return "EditorPromozioni";
		}
		return "redirect:/Utente/Login";
	}
	
	
	//AGGIUNGE UNA NUOVA PROMOZIONE
	@GetMapping("/EditorPromozioni/AggiungiPromozione")
	public String aggiungiPromozione(Promozione promozione) {
		return "AggiungiPromozione"; 
	}
	
	@PostMapping("/EditorPromozioni/AggiungiPromozione")
	public String postAggiungiPromozione(@Valid Promozione promozione, BindingResult bindingResult){
        if(bindingResult.hasErrors()){
            return "AggiungiPromozione";
        }
		promozioneRepository.save(promozione);
		return "redirect:/Admin/EditorPromozioni/ModificaPromozione/"+promozione.getIdPromozione();
    }
	
	
	//MODIFICA LA PROMOZIONE SELEZIONATA
	@RequestMapping("/EditorPromozioni/ModificaPromozione/{id}")
	public String modificaPromozione(@PathVariable("id") int id, Model model) {
		Promozione promozione = promozioneRepository.findByIdPromozione(id);

        if (promozione != null){
        	model.addAttribute("promozione", promozione);
        	List <Prodotto> listaProdotti = prodottoRepository.findAll();
        	model.addAttribute("prodotti", listaProdotti);
        	return "ModificaPromozione";
        }
        else {
            return "redirect:/Admin/EditorPromozioni";
        }
	}
	
	@PostMapping("/EditorPromozioni/ModificaPromozione")
	public String postModificaPromozione(@Valid Promozione promozione, BindingResult bindingResult, Model model) {
        if(bindingResult.hasErrors()){
            return "ModificaPromozione";
        }
        promozioneRepository.save(promozione);
        return "redirect:/Admin/EditorPromozioni/ModificaPromozione/"+promozione.getIdPromozione();
	}
	
	
	//TASTO RIMUOVI SOTTO I PRODOTTI ATTUALMENTE PRESENTI NELLA PROMOZIONE SELEZIONATA
	@GetMapping("/EditorPromozioni/ModificaPromozione/{id}/{idProdotto}")
	public String rimuoviProdottoDaPromozione(@PathVariable("id") int id,@PathVariable("idProdotto") int idProdotto) {
		ListaPromo l = listaPromoRepository.getSingolaListaPromo(idProdotto, id);
		listaPromoRepository.deleteById(l.getIdListaPromo());
		return "redirect:/Admin/EditorPromozioni/ModificaPromozione/"+id;
	}

	
	//RIMUOVE LA PROMOZIONE SELEZIONATA
	@GetMapping("/EditorPromozioni/RimuoviPromozione/{id}")
	public String rimuoviPromozione(@PathVariable("id") int id) {
		promozioneRepository.deleteById(id);
		return "redirect:/Admin/EditorPromozioni";
	}
    
	
	//LISTA DI TUTTE LE MARCHE
	@GetMapping("/EditorMarche")
	public String editorMarche(Model model, HttpSession session){
		Utente utente = (Utente) session.getAttribute("utenteAutenticato");
		if (utente != null && utente.getRuolo().getIdRuolo() == 2) {
		model.addAttribute("marca", marcaRepository.findAll());
		return "EditorMarche";
		}
		return "redirect:/Utente/Login";
	}
	
	
	//AGGIUNGE UNA MARCA
	@GetMapping("/EditorMarche/AggiungiMarca")
	public String aggiungiMarca(Marca marca) {
		return "AggiungiMarca"; 
	}
	
	@PostMapping("/EditorMarche/AggiungiMarca")
	public String postAggiungiMarca(@Valid Marca marca, BindingResult bindingResult){
        if(bindingResult.hasErrors()){
            return "AggiungiMarca";
        }
		marcaRepository.save(marca);
		return "redirect:/Admin/EditorMarche";
    }
	
	
	//MODIFICA LA MARCA SELEZIONATA
	@GetMapping("/EditorMarche/ModificaMarca/{id}")
	public String modificaMarca(@PathVariable("id") int id, Model model) {
		Marca marca = marcaRepository.findByIdMarca(id);

        	model.addAttribute("marca", marca);
        	
            return "ModificaMarca";
	}
	
	@PostMapping("/EditorMarche/ModificaMarca")
	public String postModificaMarca(@Valid Marca marca, BindingResult bindingResult, Model model) {
        if(bindingResult.hasErrors()){
            return "ModificaMarca";
        }
        marcaRepository.save(marca);
		
        return "redirect:/Admin/EditorMarche";
	}
	
	
	//ELIMINA LA MARCA SELEZIONATA
	@GetMapping("/EditorMarche/RimuoviMarca/{id}")
	public String rimuoviMarca(@PathVariable("id") int id) {
		marcaRepository.deleteById(id);
		return "redirect:/Admin/EditorMarche";
	}
}	



