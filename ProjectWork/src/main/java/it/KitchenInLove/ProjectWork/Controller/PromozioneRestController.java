package it.KitchenInLove.ProjectWork.Controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import it.KitchenInLove.ProjectWork.Dao.ListaPromoDao;
import it.KitchenInLove.ProjectWork.Dao.ProdottoDao;
import it.KitchenInLove.ProjectWork.Dao.PromozioneDao;
import it.KitchenInLove.ProjectWork.Dao.ShoppingListDao;
import it.KitchenInLove.ProjectWork.Model.ListaPromo;
import it.KitchenInLove.ProjectWork.Model.Prodotto;
import it.KitchenInLove.ProjectWork.Model.Promozione;

@RestController
public class PromozioneRestController {

	@Autowired
	PromozioneDao promozioneRepository;
	@Autowired
	ProdottoDao prodottoRepository;
	@Autowired
	ListaPromoDao listaPromoRepository;
	@Autowired
	ShoppingListDao shoppingListRepository;
		
		//LISTA DEI PRODOTTI ASSOCIATI AD UNA PROMOZIONE
		//[{"idPromozione":8,"idProdotto":6},{"idPromozione":8,"idProdotto":7},{"idPromozione":8,"idProdotto":5},{"idPromozione":8,"idProdotto":8}]
		@GetMapping("/Admin/ListaPromo/{id}")
		public ArrayList<Map<String, Object>> listaPromo(@PathVariable("id") int id){
			List <ListaPromo> listaPromoInPromozione = listaPromoRepository.listaPerPromozione(id);
			
			int lung = listaPromoInPromozione.size();
			ArrayList<Map<String, Object>> json = new ArrayList<Map<String, Object>>();
				for(int i = 0; i < lung; i++) {
					int idProd = listaPromoInPromozione.get(i).getProdotto().getIdProdotto();
					int idPromozione = listaPromoInPromozione.get(i).getPromozione().getIdPromozione();
					Map<String, Object> diz = new HashMap<String, Object>();
					diz.put("idProdotto", idProd);
					diz.put("idPromozione", idPromozione);
					json.add(diz);
				}
				
			return json;
		}
	
		//AGGIUNGE UN PRODOTTO AD UNA PROMOZIONE SOLO SE NON È GIÀ PRESENTE IN UNA PROMOZIONE CON DATE COINCIDENTI
		@PostMapping("/Admin/ListaPromo/{id}")
		public Map<String,String> postListaPromo(@RequestBody String stringa, @PathVariable("id") int id){	
			Map<String,String> risultatoCercaProdotto = new HashMap<String,String>();
			JSONArray array = new JSONArray(stringa);  
			String nomiProdottiInPromo = "";
			for(int i=0; i < array.length(); i++)   {  
				JSONObject object = array.getJSONObject(i);  
				int idProdotto = object.getInt("idProdotto");
				int idPromozione = object.getInt("idPromozione");
				Prodotto prodotto = prodottoRepository.findByIdProdotto(idProdotto);
				Promozione promo = promozioneRepository.findByIdPromozione(idPromozione);
				
				List <Promozione> listaPromozioni = promozioneRepository.cercaProdInPromoCorr(prodotto.getIdProdotto());
		        int cont = 0;
		        for (Promozione p : listaPromozioni) {
		        	if(   (promo.getDataFine().after(p.getDataInizio())) && (promo.getDataFine().before(p.getDataFine())) ||   (promo.getDataInizio().after(p.getDataInizio())) && (promo.getDataInizio().before(p.getDataFine()))  ||  (promo.getDataInizio().before(p.getDataInizio())) && (promo.getDataFine().after(p.getDataFine()))  ) {
		        		cont++;
		        		nomiProdottiInPromo+=prodotto.getNome() + "\n";
		        	}
		        }
		        if (cont == 0) {
		        	ListaPromo l = listaPromoRepository.getSingolaListaPromo(prodotto.getIdProdotto(), promo.getIdPromozione());
					if (l == null) {
						l = new ListaPromo();
						l.setProdotto(prodotto);
						l.setPromozione(promo);
						
					}
					listaPromoRepository.save(l);	
					risultatoCercaProdotto.put("status", "La promozione è stata salvata");
		        }
		        else {
		        	risultatoCercaProdotto.put("status", "Questi prodotti sono già presenti in promozioni con date coincidenti: " + nomiProdottiInPromo);
		        }
			}
			return risultatoCercaProdotto;
		}
}
