package it.KitchenInLove.ProjectWork.Model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "Utenti")
public class Utente {

	@Id 
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq_utente")
    @SequenceGenerator(name="seq_utente", sequenceName = "seq_utente", allocationSize = 1, initialValue= 2)
	@Column(name = "id_Utente")
    private Integer idUtente;
	
	@NotBlank(message = "è necessario inserire il nome")
    private String nome;
    
    @NotBlank(message = "è necessario inserire il cognome")
	private String cognome;
	
    @Email(message = "Email inserita non valida") //controlla la @, ma non il . !!
    @NotBlank (message = "è necessario inserire l'email")
	private String email;
	
	private String telefono;  //sistemare: se + primo carattere ok, ma resto int
	
	@Size(min=5, max=10, message = "L' username deve avere tra 5 e 10 caratteri")
    @NotBlank(message = "è necessario inserire un username")
	private String username;
    
    @Size(min=6, max=12, message = "La password deve avere tra 6 e 12 caratteri")
    @NotBlank(message = "è necessario inserire una password")
	private String password;
	
    @Transient
    private String confPassword;
    
	@OneToOne
	private Ruolo ruolo;
	
    @JsonIgnore
	@OneToMany(mappedBy = "utente", cascade = CascadeType.ALL)
	private Set <Indirizzo> listaIndirizzi = new HashSet <>();
	
    @JsonIgnore
	@OneToMany(mappedBy = "utente", cascade = CascadeType.ALL)
	private Set <ShoppingList> listaShopping = new HashSet <>();
	
	public Utente() {
	}

	public Utente(int idUtente, @NotBlank(message = "è necessario inserire il nome") String nome,
			@NotBlank(message = "è necessario inserire il cognome") String cognome,
			@Email(message = "Email inserita non valida") @NotBlank(message = "è necessario inserire l'email") String email,
			String telefono,
			@Size(min = 5, max = 10, message = "L' username deve avere tra 5 e 10 caratteri") @NotBlank(message = "è necessario inserire un username") String username,
			@Size(min = 6, max = 12, message = "La password deve avere tra 6 e 12 caratteri") @NotBlank(message = "è necessario inserire una password") String password,
			@NotBlank (message = "è necessario inserire la password di conferma") String confPassword, Ruolo ruolo, Set<Indirizzo> listaIndirizzi, Set<ShoppingList> listaShopping) {
		super();
		this.idUtente = idUtente;
		this.nome = nome;
		this.cognome = cognome;
		this.email = email;
		this.telefono = telefono;
		this.username = username;
		this.password = password;
		this.confPassword = confPassword;
		this.ruolo = ruolo;
		this.listaIndirizzi = listaIndirizzi;
		this.listaShopping = listaShopping;
	}

	public int getIdUtente() {
		return idUtente;
	}

	public void setIdUtente(int idUtente) {
		this.idUtente = idUtente;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfPassword() {
		return confPassword;
	}

	public void setConfPassword(String confPassword) {
		this.confPassword = confPassword;
	}

	public Ruolo getRuolo() {
		return ruolo;
	}

	public void setRuolo(Ruolo ruolo) {
		this.ruolo = ruolo;
	}

	public Set<Indirizzo> getListaIndirizzi() {
		return listaIndirizzi;
	}

	public void setListaIndirizzi(Set<Indirizzo> listaIndirizzi) {
		this.listaIndirizzi = listaIndirizzi;
	}

	public Set<ShoppingList> getListaShopping() {
		return listaShopping;
	}

	public void setListaShopping(Set<ShoppingList> listaShopping) {
		this.listaShopping = listaShopping;
	}
	
	@Override
	public String toString() {
		return "Utente [idUtente=" + idUtente + ", nome=" + nome + ", cognome=" + cognome + ", email=" + email
				+ ", telefono=" + telefono + ", username=" + username + ", password=" + password + ", confPassword="
				+ confPassword + ", ruolo=" + ruolo + ", listaIndirizzi=" + listaIndirizzi + ", listaShopping="
				+ listaShopping + "]";
	}

}
