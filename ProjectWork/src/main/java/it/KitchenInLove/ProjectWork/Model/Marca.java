package it.KitchenInLove.ProjectWork.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "Marche")
public class Marca {
	
	@Id 
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq_marca")
	@SequenceGenerator(name="seq_marca", sequenceName = "seq_marca", allocationSize = 1, initialValue= 1)
	@Column(name = "id_Marca")
    private int idMarca;
	
	private String nome;
	@Column (name = "grande")
	int grande;
    
	@Column (name = "immagine")
	String immagine;
	
	public Marca() {
	}

	public Marca(int idMarca, String nome, int grande, String immagine) {
		this.idMarca = idMarca;
		this.nome = nome;
		this.grande = grande;
		this.immagine = immagine;
	}

	public int getIdMarca() {
		return idMarca;
	}

	public void setIdMarca(int idMarca) {
		this.idMarca = idMarca;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public int getGrande() {
		return idMarca;
	}

	public void setGrande(int grande) {
		this.grande = grande;
	}
	
	public String getImmagine() {
		return immagine;
	}

	public void setImmagine(String immagine) {
		this.immagine = immagine;
	}

	@Override
	public String toString() {
		return "Marca [idMarca=" + idMarca + ", nome=" + nome + ", grande=" + grande + " ]";
	}
	
}
