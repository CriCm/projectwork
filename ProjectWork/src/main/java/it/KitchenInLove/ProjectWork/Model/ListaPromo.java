package it.KitchenInLove.ProjectWork.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name= "Lista_Promo")
public class ListaPromo {
	

	@Id 
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq_lista_promo")
	@SequenceGenerator(name="seq_lista_promo", sequenceName = "seq_lista_promo", allocationSize = 1, initialValue= 1)
	@Column(name = "id_lista_Promo")
    private int idListaPromo;
	
	@ManyToOne
	@JoinColumn
	private Prodotto prodotto;
	
	@ManyToOne
	@JoinColumn
	private Promozione promozione;

	public ListaPromo() {
	}

	public ListaPromo(int idListaPromo, Prodotto prodotto, Promozione promozione) {
		this.idListaPromo = idListaPromo;
		this.prodotto = prodotto;
		this.promozione = promozione;
	}

	public int getIdListaPromo() {
		return idListaPromo;
	}

	public void setIdListaPromo(int idListaPromo) {
		this.idListaPromo = idListaPromo;
	}

	public Prodotto getProdotto() {
		return prodotto;
	}

	public void setProdotto(Prodotto prodotto) {
		this.prodotto = prodotto;
	}

	public Promozione getPromozione() {
		return promozione;
	}

	public void setPromozione(Promozione promozione) {
		this.promozione = promozione;
	}
}
