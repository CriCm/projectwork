package it.KitchenInLove.ProjectWork.Model;


import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name= "Ordini")
public class Ordine {
	
	@Id 
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq_ordine")
	@SequenceGenerator(name="seq_ordine", sequenceName = "seq_ordine", allocationSize = 1, initialValue= 1)
	@Column(name = "id_Ordine")
	private int idOrdine;
	
	@OneToOne
	private StatoOrdine statoOrdine;
	
	@Column(name = "data_Creazione")
	private LocalDate dataCreazione;
	
	@Column(name = "data_Invio")
	private LocalDate dataInvio;
	
	@Column(name = "data_Consegna")
	private LocalDate dataConsegna;
	
	public Ordine() {
	}

	
	public Ordine(int idOrdine, StatoOrdine statoOrdine, LocalDate dataCreazione, LocalDate dataInvio, LocalDate dataConsegna) {
		super();
		this.idOrdine = idOrdine;
		this.statoOrdine = statoOrdine;
		this.dataCreazione = dataCreazione;
		this.dataInvio = dataInvio;
		this.dataConsegna = dataConsegna;
	}


	public int getIdOrdine() {
		return idOrdine;
	}

	public void setIdOrdine(int idOrdine) {
		this.idOrdine = idOrdine;
	}

	public StatoOrdine getStatoOrdine() {
		return statoOrdine;
	}

	public void setStatoOrdine(StatoOrdine statoOrdine) {
		this.statoOrdine = statoOrdine;
	}

	public LocalDate getDataCreazione() {
		return dataCreazione;
	}

	public void setDataCreazione(LocalDate now) {
		this.dataCreazione = now;
	}

	public LocalDate getDataInvio() {
		return dataInvio;
	}

	public void setDataInvio(LocalDate dataInvio) {
		this.dataInvio = dataInvio;
	}

	public LocalDate getDataConsegna() {
		return dataConsegna;
	}

	public void setDataConsegna(LocalDate dataConsegna) {
		this.dataConsegna = dataConsegna;
	}


	@Override
	public String toString() {
		return "Ordine [idOrdine=" + idOrdine + ", statoOrdine=" + statoOrdine + ", dataCreazione=" + dataCreazione
				+ ", dataInvio=" + dataInvio + ", dataConsegna=" + dataConsegna + "]";
	}

	
}