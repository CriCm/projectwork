package it.KitchenInLove.ProjectWork.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name= "Immagini")
public class Immagine {

	@Id 
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq_immagine")
	@SequenceGenerator(name="seq_immagine", sequenceName = "seq_immagine", allocationSize = 1, initialValue= 1)
	@Column(name = "id_Immagine")
    private int idImmagine;
	
	@Column(name = "url_Immagine")
	private String urlImmagine;
	
	@ManyToOne
	@JoinColumn
	private Prodotto prodotto;
	
	private int principale;

	public Immagine() {
	}

	public Immagine(int idImmagine, String urlImmagine, Prodotto prodotto, int principale) {
		super();
		this.idImmagine = idImmagine;
		this.urlImmagine = urlImmagine;
		this.prodotto = prodotto;
		this.principale = principale;
	}

	public int getIdImmagine() {
		return idImmagine;
	}

	public void setIdImmagine(int idImmagine) {
		this.idImmagine = idImmagine;
	}

	public String getUrlImmagine() {
		return urlImmagine;
	}

	public void setUrlImmagine(String urlImmagine) {
		this.urlImmagine = urlImmagine;
	}

	public Prodotto getProdotto() {
		return prodotto;
	}

	public void setProdotto(Prodotto prodotto) {
		this.prodotto = prodotto;
	}

	public int getPrincipale() {
		return principale;
	}

	public void setPrincipale(int principale) {
		this.principale = principale;
	}

	@Override
	public String toString() {
		return "Immagine [idImmagine=" + idImmagine + ", urlImmagine=" + urlImmagine + ", prodotto=" + prodotto
				+ ", principale=" + principale + "]";
	}
}
