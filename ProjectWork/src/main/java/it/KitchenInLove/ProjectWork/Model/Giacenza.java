package it.KitchenInLove.ProjectWork.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "Giacenza")
public class Giacenza {
	
	@Id 
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq_giacenza")
	@SequenceGenerator(name="seq_giacenza", sequenceName = "seq_giacenza", allocationSize = 1, initialValue= 1)
	@Column(name = "id_Giacenza")
	private int idGiacenza;
	
	@ManyToOne
	@JoinColumn
	private Prodotto prodotto;
	
	@ManyToOne
	@JoinColumn
	private Sede sede;
	
	@Column(name= "quantità")
	private Integer quantita;

	public Giacenza() {
	}

	public Giacenza(int idGiacenza, Prodotto prodotto, Sede sede, Integer quantita) {
		super();
		this.idGiacenza = idGiacenza;
		this.prodotto = prodotto;
		this.sede = sede;
		this.quantita = quantita;
	}

	public int getIdGiacenza() {
		return idGiacenza;
	}

	public void setIdGiacenza(int idGiacenza) {
		this.idGiacenza = idGiacenza;
	}

	public Prodotto getProdotto() {
		return prodotto;
	}

	public void setProdotto(Prodotto prodotto) {
		this.prodotto = prodotto;
	}

	public Sede getSede() {
		return sede;
	}

	public void setSede(Sede sede) {
		this.sede = sede;
	}

	public Integer getQuantita() {
		return quantita;
	}

	public void setQuantita(Integer quantita) {
		this.quantita = quantita;
	}


}
