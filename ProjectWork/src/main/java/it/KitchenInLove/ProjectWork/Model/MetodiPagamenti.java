//SI E' DECISO DI NON COLLEGARE IL SITO AD UN SISTEMA DI PAGAMENTO ED ACCORPARE IL PAGAMENTO ALLA CREAZIONE DELL'ORDINE
//QUESTA CLASSE NON è QUINDI STATA UTILIZZATA (lasciata solo per collegamento alla classe Pagamento)

package it.KitchenInLove.ProjectWork.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name= "Metodi_Pagamenti")
public class MetodiPagamenti {
	
	@Id 
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq_metodo_pagamento")
	@SequenceGenerator(name="seq_metodo_pagamento", sequenceName = "seq_metodo_pagamento", allocationSize = 1, initialValue= 1)
	@Column(name = "id_Metodo_Pagamento")
    private int idMetodoPagamento;
	
	private String nome;

	public MetodiPagamenti() {
	}

	public MetodiPagamenti(int idMetodoPagamento, String nome) {
		this.idMetodoPagamento = idMetodoPagamento;
		this.nome = nome;
	}

	public int getIdMetodoPagamento() {
		return idMetodoPagamento;
	}

	public void setIdMetodoPagamento(int idMetodoPagamento) {
		this.idMetodoPagamento = idMetodoPagamento;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public String toString() {
		return "MetodiPagamenti [idMetodoPagamento=" + idMetodoPagamento + ", nome=" + nome + "]";
	}
	
}
