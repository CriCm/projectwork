package it.KitchenInLove.ProjectWork.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name= "Ruoli")
public class Ruolo {

	@Id 
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq_ruolo")
    @SequenceGenerator(name="seq_ruolo", sequenceName = "seq_ruolo", allocationSize = 1, initialValue= 3)
	@Column(name = "id_Ruolo")
	private int idRuolo;
	
	@Column(name = "nome_Ruolo")
	private String nomeRuolo;
	
	private String descrizione;
	
	public Ruolo() {
		
	}

	public Ruolo(int idRuolo, String nomeRuolo, String descrizione) {
		this.idRuolo = idRuolo;
		this.nomeRuolo = nomeRuolo;
		this.descrizione = descrizione;
	}

	public int getIdRuolo() {
		return idRuolo;
	}

	public void setIdRuolo(int idRuolo) {
		this.idRuolo = idRuolo;
	}

	public String getNomeRuolo() {
		return nomeRuolo;
	}

	public void setNomeRuolo(String nomeRuolo) {
		this.nomeRuolo = nomeRuolo;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	@Override
	public String toString() {
		return "Ruolo [idRuolo=" + idRuolo + ", nomeRuolo=" + nomeRuolo + ", descrizione=" + descrizione + "]";
	}

}