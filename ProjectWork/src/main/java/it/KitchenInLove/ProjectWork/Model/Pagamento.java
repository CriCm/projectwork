package it.KitchenInLove.ProjectWork.Model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name= "Pagamenti")
public class Pagamento {
	
	@Id 
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq_pagamento")
	@SequenceGenerator(name="seq_pagamento", sequenceName = "seq_pagamento", allocationSize = 1, initialValue= 1)
	@Column(name = "id_Pagamento")
	private int idPagamento;

	@Column(name = "data_Pagamento")
	private LocalDate dataPagamento;

	private double importo;
	
	@OneToOne
	private TipiPagamenti tipoPagamento;

	public Pagamento() {
	}

	public Pagamento(int idPagamento, LocalDate dataPagamento, double importo, TipiPagamenti tipoPagamento) {
		this.idPagamento = idPagamento;
		this.dataPagamento = dataPagamento;
		this.importo = importo;
		this.tipoPagamento = tipoPagamento;
	}

	public int getIdPagamento() {
		return idPagamento;
	}

	public void setIdPagamento(int idPagamento) {
		this.idPagamento = idPagamento;
	}

	public LocalDate getDataPagamento() {
		return dataPagamento;
	}

	public void setDataPagamento(LocalDate ora) {
		this.dataPagamento = ora;
	}

	public double getImporto() {
		return importo;
	}

	public void setImporto(double importo) {
		this.importo = importo;
	}

	public TipiPagamenti getTipoPagamento() {
		return tipoPagamento;
	}

	public void setTipoPagamento(TipiPagamenti tipoPagamento) {
		this.tipoPagamento = tipoPagamento;
	}

	@Override
	public String toString() {
		return "Pagamento [idPagamento=" + idPagamento + ", dataPagamento=" + dataPagamento + ", importo=" + importo
				+ ", tipoPagamento=" + tipoPagamento + "]";
	}

}