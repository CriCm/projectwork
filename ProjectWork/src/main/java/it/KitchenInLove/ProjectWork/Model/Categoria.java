package it.KitchenInLove.ProjectWork.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "Categorie")
public class Categoria {
	
	@Id 
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq_categoria")
	@SequenceGenerator(name="seq_categoria", sequenceName = "seq_categoria", allocationSize = 1, initialValue= 1)
	@Column(name = "id_Categoria")
    private int idCategoria;
	
	private String nome;
	
	private String descrizione;
	
	@Column(name = "id_Genitore")
	private int idGenitore;
    
	@Column (name = "immagine")
	String immagine;
	
	
	public Categoria() {
	}

	public Categoria(int idCategoria, String nome, String descrizione, int idGenitore, String immagine) {
		this.idCategoria = idCategoria;
		this.nome = nome;
		this.descrizione = descrizione;
		this.idGenitore = idGenitore;
		this.immagine = immagine;
	}

	public int getIdCategoria() {
		return idCategoria;
	}

	public void setIdCategoria(int idCategoria) {
		this.idCategoria = idCategoria;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public int getIdGenitore() {
		return idGenitore;
	}

	public void setIdGenitore(int idGenitore) {
		this.idGenitore = idGenitore;
	}
	
	public String getImmagine() {
		return immagine;
	}

	public void setImmagine(String immagine) {
		this.immagine = immagine;
	}

	@Override
	public String toString() {
		return "Categoria [idCategoria=" + idCategoria + ", nome=" + nome + ", descrizione=" + descrizione
				+ ", idGenitore=" + idGenitore + "]";
	}

	
}
