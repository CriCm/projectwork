package it.KitchenInLove.ProjectWork.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "Stato_Ordini")
public class StatoOrdine {

	@Id 
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq_stato_ordine")
    @SequenceGenerator(name="seq_stato_ordine", sequenceName = "seq_stato_ordine", allocationSize = 1, initialValue= 1)
	@Column(name = "id_Stato_Ordine")
	private int idStatoOrdine;
	
	private String descrizione;

	public StatoOrdine() {
	}

	public StatoOrdine(int idStatoOrdine, String descrizione) {
		this.idStatoOrdine = idStatoOrdine;
		this.descrizione = descrizione;
	}

	public int getIdStatoOrdine() {
		return idStatoOrdine;
	}

	public void setIdStatoOrdine(int idStatoOrdine) {
		this.idStatoOrdine = idStatoOrdine;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	@Override
	public String toString() {
		return "StatoOrdine [idStatoOrdine=" + idStatoOrdine + ", descrizione=" + descrizione + "]";
	}
	
}
