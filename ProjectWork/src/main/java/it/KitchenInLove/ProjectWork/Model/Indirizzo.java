package it.KitchenInLove.ProjectWork.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name= "Indirizzi")
public class Indirizzo {

	@Id 
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq_indirizzo")
	@SequenceGenerator(name="seq_indirizzo", sequenceName = "seq_indirizzo", allocationSize = 1, initialValue= 1)
	@Column(name = "id_Indirizzo")
	private int idIndirizzo;
	
	private String via;
	
	@Column(name = "città")
	private String citta;
	
	private String provincia;
	
	private String cap;
	
	@ManyToOne
	@JoinColumn
	private Utente utente;
	
	private String note;

	public Indirizzo() {
	}

	public Indirizzo(int idIndirizzo, String via, String citta, String provincia, String cap, Utente utente, String note) {
		this.idIndirizzo = idIndirizzo;
		this.via = via;
		this.citta = citta;
		this.provincia = provincia;
		this.cap = cap;
		this.utente = utente;
		this.note = note;
	}

	public int getIdIndirizzo() {
		return idIndirizzo;
	}

	public void setIdIndirizzo(int idIndirizzo) {
		this.idIndirizzo = idIndirizzo;
	}

	public String getVia() {
		return via;
	}

	public void setVia(String via) {
		this.via = via;
	}

	public String getCitta() {
		return citta;
	}

	public void setCitta(String citta) {
		this.citta = citta;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public String getCap() {
		return cap;
	}

	public void setCap(String cap) {
		this.cap = cap;
	}

	public Utente getUtente() {
		return utente;
	}

	public void setUtente(Utente utente) {
		this.utente = utente;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	@Override
	public String toString() {
		return "Indirizzo [idIndirizzo=" + idIndirizzo + ", via=" + via + ", citta=" + citta + ", provincia="
				+ provincia + ", cap=" + cap + ", note=" + note + "]";
	}

	
}
