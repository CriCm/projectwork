package it.KitchenInLove.ProjectWork.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name= "Shopping_List")
public class ShoppingList{

	@Id 
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq_shopping_list")
    @SequenceGenerator(name="seq_shopping_list", sequenceName = "seq_shopping_list", allocationSize = 1, initialValue= 1)
	@Column(name = "id_Shopping_List")
    private int idShoppingList;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn
	private Prodotto prodotto;
	
  	@Column(name = "quantità")
	private Integer quantita;
	
  	@JsonIgnore
	@ManyToOne
	@JoinColumn
	private Utente utente;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn
	private Ordine ordine;

	public ShoppingList() {
	}

	
	public ShoppingList(int idShoppingList, Prodotto prodotto, int quantita, Utente utente, Ordine ordine) {
		this.idShoppingList = idShoppingList;
		this.prodotto = prodotto;
		this.quantita = quantita;
		this.utente = utente;
		this.ordine = ordine;
	}

	public int getIdShoppingList() {
		return idShoppingList;
	}

	public void setIdShoppingList(int idShoppingList) {
		this.idShoppingList = idShoppingList;
	}

	public Prodotto getProdotto() {
		return prodotto;
	}

	public void setProdotto(Prodotto prodotto) {
		this.prodotto = prodotto;
	}

	public int getQuantita() {
		return quantita;
	}

	public void setQuantita(int quantita) {
		this.quantita = quantita;
	}

	public Ordine getOrdine() {
		return ordine;
	}

	public void setOrdine(Ordine ordine) {
		this.ordine = ordine;
	}

	public Utente getUtente() {
		return utente;
	}


	public void setUtente(Utente utente) {
		this.utente = utente;
	}
	
	public double getPrezzoParziale() {
		double prezzoParziale = 0;
		if (prodotto.getPrezzoScontato()==0)
			prezzoParziale = prodotto.getPrezzo()*this.quantita;
		else
			prezzoParziale = prodotto.getPrezzoScontato()*this.quantita;
		return prezzoParziale;
	}

	@Override
	public String toString() {
		return "ShoppingList [idShoppingList=" + idShoppingList + ", prodotto=" + prodotto + ", quantita=" + quantita
				+ ", utente=" + utente + ", ordine=" + ordine + "]";
	}

}
