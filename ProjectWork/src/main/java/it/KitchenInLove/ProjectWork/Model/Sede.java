package it.KitchenInLove.ProjectWork.Model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "Sedi")
public class Sede {
	
	@Id 
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq_sede")
    @SequenceGenerator(name="seq_sede", sequenceName = "seq_sede", allocationSize = 1, initialValue= 14)
	@Column(name = "id_Sede")
    private int idSede;
	
	private String nome;
	
	private String via;
	
	@Column(name = "città")
	private String citta;
	
	private String provincia;
	
	private String cap;
	
	private String telefono;
	
	private String email;
	
	public static final int sedeOnline = 1;
	
	@OneToMany(mappedBy = "sede", cascade = CascadeType.ALL)
	private Set <Giacenza> listaGiacenza = new HashSet <>(); 

	public Sede() {
	}

	public Sede(int idSede, String nome, String via, String citta, String provincia, String cap, String telefono,
			String email, Set<Giacenza> listaGiacenza) {
		this.idSede = idSede;
		this.nome = nome;
		this.via = via;
		this.citta = citta;
		this.provincia = provincia;
		this.cap = cap;
		this.telefono = telefono;
		this.email = email;
		this.listaGiacenza = listaGiacenza;
	}

	public int getIdSede() {
		return idSede;
	}

	public void setIdSede(int idSede) {
		this.idSede = idSede;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getVia() {
		return via;
	}

	public void setVia(String via) {
		this.via = via;
	}

	public String getCitta() {
		return citta;
	}

	public void setCitta(String citta) {
		this.citta = citta;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public String getCap() {
		return cap;
	}

	public void setCap(String cap) {
		this.cap = cap;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Set<Giacenza> getListaGiacenza() {
		return listaGiacenza;
	}

	public void setListaGiacenza(Set<Giacenza> listaGiacenza) {
		this.listaGiacenza = listaGiacenza;
	}

	@Override
	public String toString() {
		return "Sede [idSede=" + idSede + ", nome=" + nome + ", via=" + via + ", citta=" + citta + ", provincia="
				+ provincia + ", cap=" + cap + ", telefono=" + telefono + ", email=" + email + ",listaGiacenza=" + listaGiacenza + "]";
	}

	
	
}
