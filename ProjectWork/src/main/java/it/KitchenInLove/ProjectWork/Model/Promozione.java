package it.KitchenInLove.ProjectWork.Model;

import java.sql.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name= "Promozioni")
public class Promozione {

	@Id 
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq_promozione")
    @SequenceGenerator(name="seq_promozione", sequenceName = "seq_promozione", allocationSize = 1, initialValue= 1)
	@Column(name = "id_Promozione")
    private int idPromozione;
	
	private String nome;
	
	private String descrizione;
	
	private String immagine;
	
	@Column(name = "data_Inizio")
	private Date dataInizio;
	
	@Column(name = "data_Fine")
	private Date dataFine;
	
	@Column(name = "percentuale_Sconto")
	private int percentualeSconto;
	
	@OneToMany(mappedBy = "promozione", cascade = CascadeType.ALL)
	private Set <ListaPromo> listaPromozioni = new HashSet <>();

	public Promozione() {
	}

	public Promozione(int idPromozione, String nome, String descrizione, String immagine, Date dataInizio,
			Date dataFine, int percentualeSconto, Set<ListaPromo> listaPromozioni) {
		this.idPromozione = idPromozione;
		this.nome = nome;
		this.descrizione = descrizione;
		this.immagine = immagine;
		this.dataInizio = dataInizio;
		this.dataFine = dataFine;
		this.percentualeSconto = percentualeSconto;
		this.listaPromozioni = listaPromozioni;
	}

	public int getIdPromozione() {
		return idPromozione;
	}

	public void setIdPromozione(int idPromozione) {
		this.idPromozione = idPromozione;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public String getImmagine() {
		return immagine;
	}

	public void setImmagine(String immagine) {
		this.immagine = immagine;
	}

	public Date getDataInizio() {
		return dataInizio;
	}

	public void setDataInizio(Date dataInizio) {
		this.dataInizio = dataInizio;
	}

	public Date getDataFine() {
		return dataFine;
	}

	public void setDataFine(Date dataFine) {
		this.dataFine = dataFine;
	}

	public int getPercentualeSconto() {
		return percentualeSconto;
	}

	public void setPercentualeSconto(int percentualeSconto) {
		this.percentualeSconto = percentualeSconto;
	}

	public Set<ListaPromo> getListaPromozioni() {
		return listaPromozioni;
	}

	public void setListaPromozioni(Set<ListaPromo> listaPromozioni) {
		this.listaPromozioni = listaPromozioni;
	}

	@Override
	public String toString() {
		return "Promozione [idPromozione=" + idPromozione + ", nome=" + nome + ", descrizione=" + descrizione
				+ ", immagine=" + immagine + ", dataInizio=" + dataInizio + ", dataFine=" + dataFine
				+ ", percentualeSconto=" + percentualeSconto + ", listaPromozioni=" + listaPromozioni + "]";
	}
	
}
