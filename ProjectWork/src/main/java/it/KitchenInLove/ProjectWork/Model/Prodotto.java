package it.KitchenInLove.ProjectWork.Model;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "Prodotti")
public class Prodotto {
	
		@Id 
		@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq_prodotto")
		@SequenceGenerator(name="seq_prodotto", sequenceName = "seq_prodotto", allocationSize = 1, initialValue= 1)
	    @Column(name = "id_Prodotto")
	    private int idProdotto;

	    @NotBlank
		private String nome;
		
		@NotBlank
		@Column(name = "breve_descrizione")
		private String breveDescrizione;
		
		@Column(name = "descrizione_Completa")
		private String descrizioneCompleta;
		
		//@NotBlank(message = "deve essere diverso da 0")
		private double prezzo;
		
		private String dimensione;
		
		private double peso;
		
		@Column(name = "rumorosità")
		private int rumorosita;
		
		private String materiale;
		
		@Column(name = "capacità")
		private int capacita;
		
		private int wattaggio;
		
		@OneToOne
		private Marca marca;
		
		@OneToOne
		private Categoria categoria;
		
		@JsonIgnore
		@OneToMany(mappedBy = "prodotto", cascade = CascadeType.ALL)
		private Set <Giacenza> listaGiacenza = new HashSet <>(); //creiamo una lista(insieme) vuota
		
		@JsonIgnore
		@OneToMany(mappedBy = "prodotto", cascade = CascadeType.ALL)
		private Set <ShoppingList> listaShoppingList = new HashSet <>();
		
		@JsonIgnore
		@OneToMany(mappedBy = "prodotto", cascade = CascadeType.ALL)
		private Set <ListaPromo> listaPromozioni = new HashSet <>();
		
		@JsonIgnore
		@OneToMany(mappedBy = "prodotto", cascade = CascadeType.ALL)
		private Set <Immagine> listaImmagini = new HashSet <>();
		
		@Transient
		public boolean esaurito = false;
		
		public Prodotto() {
		}

		public Prodotto(int idProdotto, @NotBlank String nome, @NotBlank String breveDescrizione,
				String descrizioneCompleta, double prezzo, String dimensione, double peso, int rumorosita,
				String materiale, int capacita, int wattaggio, Marca marca, Categoria categoria,
				Set<Giacenza> listaGiacenza, Set<ShoppingList> listaShoppingList, Set<ListaPromo> listaPromozioni,
				Set<Immagine> listaImmagini) {
			this.idProdotto = idProdotto;
			this.nome = nome;
			this.breveDescrizione = breveDescrizione;
			this.descrizioneCompleta = descrizioneCompleta;
			this.prezzo = prezzo;
			this.dimensione = dimensione;
			this.peso = peso;
			this.rumorosita = rumorosita;
			this.materiale = materiale;
			this.capacita = capacita;
			this.wattaggio = wattaggio;
			this.marca = marca;
			this.categoria = categoria;
			this.listaGiacenza = listaGiacenza;
			this.listaShoppingList = listaShoppingList;
			this.listaPromozioni = listaPromozioni;
			this.listaImmagini = listaImmagini;
		}

		public int quantitaGiacenzaOnline() {
			int qty = 0;
			for (Giacenza g : listaGiacenza) {
				if(g.getSede().getIdSede() ==1)
					qty = g.getQuantita();
			}
			return qty;
		}
		
		public int getIdProdotto() {
			return idProdotto;
		}

		public void setIdProdotto(int idProdotto) {
			this.idProdotto = idProdotto;
		}

		public String getNome() {
			return nome;
		}

		public void setNome(String nome) {
			this.nome = nome;
		}

		public String getBreveDescrizione() {
			return breveDescrizione;
		}

		public void setBreveDescrizione(String breveDescrizione) {
			this.breveDescrizione = breveDescrizione;
		}

		public String getDescrizioneCompleta() {
			return descrizioneCompleta;
		}

		public void setDescrizioneCompleta(String descrizioneCompleta) {
			this.descrizioneCompleta = descrizioneCompleta;
		}

		public double getPrezzo() {
			return prezzo;
		}

		public void setPrezzo(double prezzo) {
			this.prezzo = prezzo;
		}
		
		public double getPrezzoScontato(double prezzo, int percentualeSconto) {
			return (prezzo/100 *(100- percentualeSconto));
		}
        
		public double getPrezzoScontato () {
			if (listaPromozioni == null) 
				return 0;
			List<ListaPromo> promozioni = new ArrayList<ListaPromo>(listaPromozioni);
			Date oggi = new Date();
			if (promozioni.size()<= 0) {
				return 0;
			}			
			for(ListaPromo i: promozioni) {
				if((i.getPromozione().getDataFine()).after(oggi) && (i.getPromozione().getDataInizio()).before(oggi)) {
					int percent = i.getPromozione().getPercentualeSconto();
					double prezzoscontato = prezzo/100*(100 - percent);
					return prezzoscontato;
				}
			}
			return 0;
		}
		
		public String getDimensione() {
			return dimensione;
		}

		public void setDimensione(String dimensione) {
			this.dimensione = dimensione;
		}

		public double getPeso() {
			return peso;
		}

		public void setPeso(double peso) {
			this.peso = peso;
		}

		public int getRumorosita() {
			return rumorosita;
		}

		public void setRumorosita(int rumorosita) {
			this.rumorosita = rumorosita;
		}

		public String getMateriale() {
			return materiale;
		}

		public void setMateriale(String materiale) {
			this.materiale = materiale;
		}

		public int getCapacita() {
			return capacita;
		}

		public void setCapacita(int capacita) {
			this.capacita = capacita;
		}

		public int getWattaggio() {
			return wattaggio;
		}

		public void setWattaggio(int wattaggio) {
			this.wattaggio = wattaggio;
		}

		public Marca getMarca() {
			return marca;
		}

		public void setMarca(Marca marca) {
			this.marca = marca;
		}

		public Categoria getCategoria() {
			return categoria;
		}

		public void setCategoria(Categoria categoria) {
			this.categoria = categoria;
		}

		public Set<Giacenza> getListaGiacenza() {
			return listaGiacenza;
		}

		public void setListaGiacenza(Set<Giacenza> listaGiacenza) {
			this.listaGiacenza = listaGiacenza;
		}

		public Set<ShoppingList> getListaShoppingList() {
			return listaShoppingList;
		}

		public void setListaShoppingList(Set<ShoppingList> listaShoppingList) {
			this.listaShoppingList = listaShoppingList;
		}
		
		//questo equivale al metodo precedente (poi su questa ShoppingList si chiama il metodo.getQuantita..
		//il problema è sempre che cicla le shoppingList del prodotto, ma non prende la quantità di quello inserito nel carello (dove ordine==null)
		//<label th:text="${Prodotto.sl().getQuantita()}"></label>
		/*public ShoppingList sl() {
			List <ShoppingList> lista = new ArrayList <ShoppingList>(listaShoppingList);
			if (lista.size()>=0) {
			for (ShoppingList i: listaShoppingList) 
			return i;
			}
			return null;
		}*/
		
		public Set<ListaPromo> getListaPromozioni() {
			return listaPromozioni;
		}

		public void setListaPromozioni(Set<ListaPromo> listaPromozioni) {
			this.listaPromozioni = listaPromozioni;
		}

		public Set<Immagine> getListaImmagini() {
			return listaImmagini;
		}

		public Immagine getImmaginePrincipale() {
			for (Immagine i: listaImmagini) {
				if(i.getPrincipale()==1) {
					return i;
				}
			}
			return null;
		}
				
		
		public void setListaImmagini(Set<Immagine> listaImmagini) {
			this.listaImmagini = listaImmagini;
		}
        
		
		/*public void isOutOfStock(List<Object[]>  objs) {
			esaurito = false;
		      for(Object[]  o: objs) {
		            //if (o.get(0) instanceof Prodotto) {
		                  Prodotto p = (Prodotto)o[0];
		                  if (p.getIdProdotto() == this.idProdotto) {
		                        Giacenza g = (Giacenza)o[1];
		                        if (g ==  null) 
		                              esaurito = true;
		                        else if (g.getQuantita() == null || g.getQuantita() <=0 || g.getSede().getIdSede() != 1)
		                        	  esaurito = true;
		                  }
		            //}
		      }
		}
		
		public static List<Prodotto>  getListaProdottiDaGiacenza(List<Object[]>  objs) {
		      List<Prodotto>  lista = new ArrayList<Prodotto>();
		      for(Object[]  o: objs) {
		            if (o[0] instanceof Prodotto) {
		                  Prodotto p = (Prodotto)o[0];
		                  p.isOutOfStock(objs);
		                  lista.add(p);
		            }
		      }
		      return lista;
		}*/
        
		public boolean isEsaurito() {
			return esaurito;
		}

		public void setEsaurito(boolean esaurito) {
			this.esaurito = esaurito;
		}

		@Override
		public String toString() {
			return "Prodotto [idProdotto=" + idProdotto + ", nome=" + nome + ", breveDescrizione=" + breveDescrizione
					+ ", descrizioneCompleta=" + descrizioneCompleta + ", prezzo=" + prezzo + ", dimensione="
					+ dimensione + ", peso=" + peso + ", rumorosita=" + rumorosita + ", materiale=" + materiale
					+ ", capacita=" + capacita + ", wattaggio=" + wattaggio + ", marca=" + marca + ", categoria="
					+ categoria + "]";
		}

}	
