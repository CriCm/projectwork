//SI E' DECISO DI NON COLLEGARE IL SITO AD UN SISTEMA DI PAGAMENTO ED ACCORPARE IL PAGAMENTO ALLA CREAZIONE DELL'ORDINE
//QUESTA CLASSE NON è QUINDI STATA UTILIZZATA (lasciata solo per collegamento alla classe Pagamento)

package it.KitchenInLove.ProjectWork.Model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name= "Tipi_Pagamenti")
public class TipiPagamenti {
	
	@Id 
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq_tipiPagamento")
    @SequenceGenerator(name="seq_tipiPagamento", sequenceName = "seq_tipiPagamento", allocationSize = 1, initialValue= 1)
	@Column(name = "id_tipo_pagamento")
    private int idTipoPagamento;
	
	@Column(name = "nome_Titolare")
	private String nomeTitolare;

	@Column(name = "numero_Carta")
	private int numeroCarta;
	
	private Date scadenza;
	
	private int cvv;
	
	@OneToOne
	private MetodiPagamenti metodoPagamento;

	public TipiPagamenti() {
	}

	public TipiPagamenti(int idTipoPagamento, String nomeTitolare, int numeroCarta, Date scadenza, int cvv, MetodiPagamenti metodoPagamento) {
		this.idTipoPagamento = idTipoPagamento;
		this.nomeTitolare = nomeTitolare;
		this.numeroCarta = numeroCarta;
		this.scadenza = scadenza;
		this.cvv = cvv;
		this.metodoPagamento = metodoPagamento;
	}

	public int getIdTipoPagamento() {
		return idTipoPagamento;
	}

	public void setIdTipoPagamento(int idTipoPagamento) {
		this.idTipoPagamento = idTipoPagamento;
	}

	public String getNomeTitolare() {
		return nomeTitolare;
	}

	public void setNomeTitolare(String nomeTitolare) {
		this.nomeTitolare = nomeTitolare;
	}

	public int getNumeroCarta() {
		return numeroCarta;
	}

	public void setNumeroCarta(int numeroCarta) {
		this.numeroCarta = numeroCarta;
	}

	public Date getScadenza() {
		return scadenza;
	}

	public void setScadenza(Date scadenza) {
		this.scadenza = scadenza;
	}

	public int getCvv() {
		return cvv;
	}

	public void setCvv(int cvv) {
		this.cvv = cvv;
	}

	public MetodiPagamenti getMetodoPagamento() {
		return metodoPagamento;
	}

	public void setMetodoPagamento(MetodiPagamenti metodoPagamento) {
		this.metodoPagamento = metodoPagamento;
	}

	@Override
	public String toString() {
		return "TipiPagamenti [idTipoPagamento=" + idTipoPagamento + ", nomeTitolare=" + nomeTitolare + ", numeroCarta="
				+ numeroCarta + ", scadenza=" + scadenza + ", cvv=" + cvv + ", metodoPagamento=" + metodoPagamento
				+ "]";
	}
	
}